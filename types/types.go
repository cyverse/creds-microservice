package types

import (
	"gitlab.com/cyverse/cacao-common/service"
)

// ListQueryData is an intermediate struct that is used for unmarshalling list operation request.
// List request is marshal as service.CredentialModel in service client (cacao-common/service), but is unmarshal as this struct.
type ListQueryData struct {
	Actor    string `json:"actor"`
	Emulator string `json:"emulator"`

	Username   string
	Type       service.CredentialType
	IsSystem   *bool
	Disabled   *bool
	Visibility service.VisibilityType
	Tags       map[string]string
}

// CredUpdate contains the necessary info to perform a update in storage adapter
type CredUpdate struct {
	SessionActor    string
	SessionEmulator string
	Username        string // owner
	ID              string
	Update          service.CredentialUpdate
}

// ApplyCredentialUpdate ...
func ApplyCredentialUpdate(cred service.CredentialModel, update service.CredentialUpdate) service.CredentialModel {
	if update.Name != nil {
		cred.Name = *update.Name
	}
	if update.Value != nil {
		cred.Value = *update.Value
	}
	if update.Description != nil {
		cred.Description = *update.Description
	}
	if update.Disabled != nil {
		cred.Disabled = *update.Disabled
	}
	if update.Visibility != nil {
		cred.Visibility = *update.Visibility
	}
	if update.UpdateOrAddTags != nil {
		if len(cred.Tags) == 0 {
			cred.Tags = update.UpdateOrAddTags
		} else {
			for name, value := range update.UpdateOrAddTags {
				cred.Tags[name] = value
			}
		}
	}
	if len(update.DeleteTags) > 0 {
		for name := range update.DeleteTags {
			delete(cred.Tags, name)
		}
	}
	return cred
}
