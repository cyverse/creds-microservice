package types

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Config is the configuration settings, which can be used by the Domain object or Adapters
type Config struct {
	VaultAddress string `envconfig:"VAULT_ADDRESS" default:"http://127.0.0.1:8200"`
	VaultToken   string `envconfig:"VAULT_TOKEN" default:"s.Nq9oyXBwpermKn5ZpMqS23r4"`

	PostgresHost                  string `envconfig:"POSTGRES_HOST"`
	PostgresUsername              string `envconfig:"POSTGRES_USER"`
	PostgresPassword              string `envconfig:"POSTGRES_PASSWORD"`
	PostgresDatabase              string `envconfig:"POSTGRES_DB"`
	PostgresSSL                   bool   `envconfig:"POSTGRES_SSL" default:"false"`
	PostgresCreateTablesIfMissing bool   `envconfig:"POSTGRES_CREATE_TABLES_IF_MISSING" default:"true"` // create tables on startup if missing
	PostgresEncryptionKeyBase64   string `envconfig:"POSTGRES_ENCRYPTION_KEY"`

	Messaging messaging2.NatsStanMsgConfig

	UseOpenSSHKeyGen                  bool   `envconfig:"USE_OPENSSH_KEYGEN"`
	KeyGenerationTmpDirectory         string `envconfig:"KEY_GEN_TMP_DIR" default:"/tmp/keygen"`
	CredentialGenerationRequestPerSec uint16 `envconfig:"CRED_GEN_RPS" default:"10"`

	// set the log level
	LogLevel string `envconfig:"LOG_LEVEL" default:"debug"`
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
// TODO: TEMPLATE: should figure out a consistent way of generating a unique but repeatable nats client id for the case when
// a microservice is part of a ReplicaSet. It might be the case that the same client doesn't need the same client id
// as long as the clients are part of the same qgroup and durable name
func (c *Config) ProcessDefaults() {
	log.Debug("ProcessDefaults() started")
	if c.Messaging.ClientID == "" {
		c.Messaging.ClientID = DefaultNatsClientID
	}

	l, err := log.ParseLevel(c.LogLevel)
	if err != nil {
		log.Warn("Could not parse config loglevel, setting to 'info', c.LogLevel was '" + c.LogLevel + "'")
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(l)
	}
}

// Override some config values
func (c *Config) Override() {
	c.Messaging.QueueGroup = defaultNatsQGroup
	c.Messaging.DurableName = defaultNatsDurableName
	c.Messaging.WildcardSubject = defaultNatsCoreSubject
}
