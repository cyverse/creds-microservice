# syntax=docker/dockerfile:1
FROM golang:1.23-bookworm as base
ARG SKAFFOLD_GO_GCFLAGS
COPY ./ /creds-microservice/
WORKDIR /creds-microservice/
RUN --mount=type=cache,target=/root/.cache/go-build eval go build -gcflags="${SKAFFOLD_GO_GCFLAGS}"

FROM gcr.io/distroless/base-debian12:nonroot
ARG GOTRACEBACK
ENV GOTRACEBACK="$GOTRACEBACK"
COPY --from=base /creds-microservice/ /
CMD ["/creds-microservice"]
