package adapters

import (
	"bytes"
	"crypto"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/adapters/storageschema"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	//lint:ignore SA1019 openpgp is only used in tests to verify pgcrypto
	"golang.org/x/crypto/openpgp"
	//lint:ignore SA1019 openpgp is only used in tests to verify pgcrypto
	"golang.org/x/crypto/openpgp/packet"
)

func generateEncryptionKey() []byte {
	aesKey := make([]byte, 32)
	n, err := rand.Read(aesKey)
	if err != nil {
		panic(err)
	}
	if n != 32 {
		panic(n)
	}
	return aesKey
}

func TestPostgresStorage_Get(t *testing.T) {
	if skipIfPostgresEnvNotSet(t) {
		return
	}

	storageCreator := func(timeSrc ports.TimeSrc) ports.PersistentStoragePort {
		cfg := types.Config{
			PostgresHost:                  os.Getenv("POSTGRES_HOST"),
			PostgresUsername:              os.Getenv("POSTGRES_USER"),
			PostgresPassword:              os.Getenv("POSTGRES_PASSWORD"),
			PostgresDatabase:              os.Getenv("POSTGRES_DB"),
			PostgresCreateTablesIfMissing: true,
			PostgresEncryptionKeyBase64:   base64.StdEncoding.EncodeToString(generateEncryptionKey()),
		}
		storage := &PostgresStorage{
			getCurrentTime: timeSrc,
		}
		err := storage.Init(cfg)
		if err != nil {
			t.Log(err)
			panic(err)
		}
		err = storage.InitDatabase()
		if err != nil {
			// create tables and ignore error in case already created
			t.Log(err)
			panic(err)
		}

		_, err = storage.db.Exec("DELETE FROM credential_tags;")
		if err != nil {
			panic(err)
		}
		_, err = storage.db.Exec("DELETE FROM credential;")
		if err != nil {
			panic(err)
		}

		return storage
	}
	testPersistentStorage(t, storageCreator)
	testPersistentStorageExtra(t, storageCreator)
}

// return a constructor for PostgresStorage for integration test (require a real instance of postgres)
func newPsqlStorageCreator(t *testing.T) func(timeSrc ports.TimeSrc) ports.PersistentStoragePort {
	storageCreator := func(timeSrc ports.TimeSrc) ports.PersistentStoragePort {
		cfg := types.Config{
			PostgresHost:                  os.Getenv("POSTGRES_HOST"),
			PostgresUsername:              os.Getenv("POSTGRES_USER"),
			PostgresPassword:              os.Getenv("POSTGRES_PASSWORD"),
			PostgresDatabase:              os.Getenv("POSTGRES_DB"),
			PostgresSSL:                   false,
			PostgresCreateTablesIfMissing: true,
			PostgresEncryptionKeyBase64:   base64.StdEncoding.EncodeToString(generateEncryptionKey()),
		}
		storage := &PostgresStorage{
			getCurrentTime: timeSrc,
		}
		err := storage.Init(cfg)
		if err != nil {
			t.Log(err)
			panic(err)
		}
		err = storage.InitDatabase()
		if err != nil {
			// create tables and ignore error in case already created
			t.Log(err)
			panic(err)
		}

		_, err = storage.db.Exec("DELETE FROM credential_tags;")
		if err != nil {
			panic(err)
		}
		_, err = storage.db.Exec("DELETE FROM credential;")
		if err != nil {
			panic(err)
		}

		return storage
	}
	return storageCreator
}

func TestPostgresStorage_Update(t *testing.T) {
	if skipIfPostgresEnvNotSet(t) {
		return
	}

	storageCreator := newPsqlStorageCreator(t)
	now := time.Now()
	getCurrentTime := func() time.Time {
		return now
	}

	t.Run("empty struct", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		var credUpdate types.CredUpdate
		err := ps.Update(credUpdate)
		if !assert.Error(t, err) {
			return
		}
	})
	t.Run("no update", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}
		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update:          service.CredentialUpdate{},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                originalCred.ID,
			Name:              originalCred.Name,
			Username:          originalCred.Username,
			Type:              originalCred.Type,
			Value:             originalCred.Value,
			Description:       originalCred.Description,
			IsSystem:          originalCred.IsSystem,
			IsHidden:          originalCred.IsHidden,
			Disabled:          originalCred.Disabled,
			Visibility:        originalCred.Visibility,
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("cred not found", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		// credential is not created, Update() should return not found
		var credUpdate = types.CredUpdate{
			SessionActor:    "foobar",
			SessionEmulator: "",
			Username:        "foobar",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Name: stringPtr("does not matter"),
			},
		}
		err := ps.Update(credUpdate)
		if !assert.Error(t, err) {
			return
		}
		_, ok := err.(*service.CacaoNotFoundError)

		assert.Truef(t, ok, "error is not of type *service.CacaoNotFoundError, %s, %s", reflect.TypeOf(err).String(), err.Error())
	})
	t.Run("update name", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Name: stringPtr("newName"),
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, "newName", refetchedCred.Name)
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                originalCred.ID,
			Name:              "newName",
			Username:          originalCred.Username,
			Type:              originalCred.Type,
			Value:             originalCred.Value,
			Description:       originalCred.Description,
			IsSystem:          originalCred.IsSystem,
			IsHidden:          originalCred.IsHidden,
			Disabled:          originalCred.Disabled,
			Visibility:        originalCred.Visibility,
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("update description (emulated)", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "testuser456",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Description: stringPtr("newDescription"),
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, "newDescription", refetchedCred.Description)
		assert.Equal(t, "testuser456", refetchedCred.UpdatedEmulatorBy)
	})
	t.Run("update value", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Value: stringPtr("new-val"),
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, "new-val", refetchedCred.Value)
	})
	t.Run("disable cred", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		disabled := true
		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Disabled: &disabled,
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, true, refetchedCred.Disabled)
	})
	t.Run("add 1 tag", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				UpdateOrAddTags: map[string]string{
					"tagName123": "tagValue123",
				},
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, map[string]string{
			"tagName123": "tagValue123",
		}, refetchedCred.Tags)
		assert.Equal(t, service.CredentialModel{
			Session:     service.Session{},
			ID:          originalCred.ID,
			Name:        originalCred.Name,
			Username:    originalCred.Username,
			Type:        originalCred.Type,
			Value:       originalCred.Value,
			Description: originalCred.Description,
			IsSystem:    originalCred.IsSystem,
			IsHidden:    originalCred.IsHidden,
			Disabled:    originalCred.Disabled,
			Visibility:  originalCred.Visibility,
			Tags: map[string]string{
				"tagName123": "tagValue123",
			},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("delete tags 1", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		defer ps.(*PostgresStorage).Close()

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
			Tags: map[string]string{
				"tagName123": "tagValue123",
			},
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				DeleteTags: map[string]struct{}{
					"tagName123": {},
				},
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                originalCred.ID,
			Name:              originalCred.Name,
			Username:          originalCred.Username,
			Type:              originalCred.Type,
			Value:             originalCred.Value,
			Description:       originalCred.Description,
			IsSystem:          originalCred.IsSystem,
			IsHidden:          originalCred.IsHidden,
			Disabled:          originalCred.Disabled,
			Visibility:        originalCred.Visibility,
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("delete tags that does not exist", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				DeleteTags: map[string]struct{}{
					"tagName123": {},
				},
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) { // delete tags that does not exist should NOT result in error
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:     service.Session{},
			ID:          originalCred.ID,
			Name:        originalCred.Name,
			Username:    originalCred.Username,
			Type:        originalCred.Type,
			Value:       originalCred.Value,
			Description: originalCred.Description,
			IsSystem:    originalCred.IsSystem,
			IsHidden:    originalCred.IsHidden,
			Disabled:    originalCred.Disabled,
			Visibility:  originalCred.Visibility,
			Tags:        map[string]string{},
			CreatedAt:   storageschema.MilliSecPrecision(now),
			// Note even though there is no update, we should update the timestamp
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
	t.Run("modify tags & name", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		originalCred := service.CredentialModel{
			Session:     service.Session{},
			ID:          "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:        "credname123",
			Username:    "testuser123",
			Type:        "generic",
			Value:       "value123",
			Description: "description123",
			Tags: map[string]string{
				"tagName123": "tagValue123",
				"tagName456": "tagValue456",
				"badTag123":  "badTag",
			},
		}
		// create cred
		err := ps.Create(originalCred)
		if err != nil {
			panic(err)
		}

		var credUpdate = types.CredUpdate{
			SessionActor:    "testuser123",
			SessionEmulator: "",
			Username:        "testuser123",
			ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
			Update: service.CredentialUpdate{
				Name: stringPtr("newName"),
				UpdateOrAddTags: map[string]string{
					"goodTag123": "foobar1",         // add new tag
					"goodTag456": "foobar4",         // add new tag
					"goodTag789": "foobar7",         // add new tag
					"tagName123": "diffTagValue123", // update tag value of existing tag
				},
				DeleteTags: map[string]struct{}{
					"badTag123": {}, // delete existing tag
					"badTag456": {}, // delete non-existing tag
				},
			},
		}
		err = ps.Update(credUpdate)
		if !assert.NoError(t, err) {
			return
		}

		// re-fetch to check if name is updated
		refetchedCred, err := ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:     service.Session{},
			ID:          originalCred.ID,
			Name:        "newName",
			Username:    originalCred.Username,
			Type:        originalCred.Type,
			Value:       originalCred.Value,
			Description: originalCred.Description,
			IsSystem:    originalCred.IsSystem,
			IsHidden:    originalCred.IsHidden,
			Disabled:    originalCred.Disabled,
			Visibility:  originalCred.Visibility,
			Tags: map[string]string{
				"tagName123": "diffTagValue123",
				"tagName456": "tagValue456",
				"goodTag456": "foobar4",
				"goodTag789": "foobar7",
				"goodTag123": "foobar1",
			},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now), // Note because the time source we used always return the same time, the creation and update timestamp are the same
			UpdatedBy:         "testuser123",
			UpdatedEmulatorBy: "",
		}, refetchedCred)
	})
}

// Testing the openpgp encryption in postgres (pgcrypto extension), ensure we can decrypt with golang function.
func TestPostgresStorage_Encryption(t *testing.T) {
	if skipIfPostgresEnvNotSet(t) {
		return
	}

	storageCreator := newPsqlStorageCreator(t)
	now := time.Now()
	getCurrentTime := func() time.Time {
		return now
	}

	t.Run("read-with-diff-key", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		err := ps.Create(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:     "cred1",
			Username: "testuser123",
			Type:     "generic",
			Value:    "val_0123456789",
		})
		if !assert.NoError(t, err) {
			return
		}

		// change key
		psImpl := ps.(*PostgresStorage)
		newKey := generateEncryptionKey()
		copy(psImpl.encryptionKey[:], newKey)

		_, err = ps.Get(service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Username: "testuser123",
		})
		if !assert.Error(t, err) {
			// we should error, since key is different
			return
		}
		if !assert.True(t, errorIsWrongKey(err)) {
			return
		}
	})

	t.Run("read-ciphertext-value", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)

		var creationReq = service.CredentialModel{
			Session:  service.Session{},
			ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
			Name:     "cred1",
			Username: "testuser123",
			Type:     "generic",
			Value:    "val_0123456789",
		}
		err := ps.Create(creationReq)
		if !assert.NoError(t, err) {
			return
		}

		// read the ciphertext
		psImpl := ps.(*PostgresStorage)
		row := psImpl.db.QueryRow(`SELECT value from credential where id=$1`, creationReq.ID)
		if err := row.Err(); !assert.NoError(t, err) {
			return
		}
		var credValueCiphertext []byte
		err = row.Scan(&credValueCiphertext)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotEmpty(t, credValueCiphertext)
		assert.NotEqual(t, []byte(creationReq.Value), credValueCiphertext) // make sure ciphertext is the same as plaintext

		// decrypt using SQL
		row = psImpl.db.QueryRow(`SELECT pgp_sym_decrypt_bytea($1, $2, 'cipher-algo=aes256');`, credValueCiphertext, psImpl.passphraseFromKey())
		if err := row.Err(); !assert.NoError(t, err) {
			return
		}
		var decryptedCredValue []byte
		err = row.Scan(&decryptedCredValue)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, creationReq.Value, string(decryptedCredValue))

		// decrypt using openpgp package in Golang
		openpgpDecrypted, err := openpgpDecrypt(credValueCiphertext, []byte(psImpl.passphraseFromKey()))
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, creationReq.Value, string(openpgpDecrypted))
	})

	t.Run("psql-then-openpgp", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		psImpl := ps.(*PostgresStorage)

		plaintext := []byte("val_0123456789")
		passphrase := psImpl.passphraseFromKey()
		row := psImpl.db.QueryRow(`SELECT pgp_sym_encrypt_bytea($1, $2, 'cipher-algo=aes256');`, plaintext, passphrase)
		if err := row.Err(); !assert.NoError(t, err) {
			return
		}
		var psqlCiphertext []byte
		err := row.Scan(&psqlCiphertext)
		if !assert.NoError(t, err) {
			return
		}

		openpgpPlaintext, err := openpgpDecrypt(psqlCiphertext, []byte(passphrase))
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, plaintext, openpgpPlaintext)
	})

	t.Run("openpgp-then-psql", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		psImpl := ps.(*PostgresStorage)

		plaintext := []byte("val_0123456789")
		passphrase := psImpl.passphraseFromKey()
		openpgpCiphertext, err := openpgpEncrypt(plaintext, []byte(passphrase))
		if err != nil {
			return
		}
		row := psImpl.db.QueryRow(`SELECT pgp_sym_decrypt_bytea($1, $2, 'cipher-algo=aes256');`, openpgpCiphertext, passphrase)
		if err := row.Err(); !assert.NoError(t, err) {
			return
		}
		var psqlPlaintext []byte
		err = row.Scan(&psqlPlaintext)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, psqlPlaintext, plaintext)
	})

	t.Run("psql-then-psql", func(t *testing.T) {
		ps := storageCreator(getCurrentTime)
		psImpl := ps.(*PostgresStorage)

		plaintext := []byte("val_0123456789")
		passphrase := psImpl.passphraseFromKey()
		row := psImpl.db.QueryRow(`SELECT pgp_sym_decrypt_bytea(pgp_sym_encrypt_bytea($1, $2, 'cipher-algo=aes256'), $2, 'cipher-algo=aes256');`, plaintext, passphrase)
		if err := row.Err(); !assert.NoError(t, err) {
			return
		}
		var psqlPlaintext []byte
		err := row.Scan(&psqlPlaintext)
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, psqlPlaintext, plaintext)
	})

	t.Run("large-value", func(t *testing.T) {
		// Get an idea about what the max length of plaintext value can be.
		ps := storageCreator(getCurrentTime)

		// set the initial value of i higher to speed things up
		for i := 15000; i < 10000; i++ {
			var creationReq = service.CredentialModel{
				Session:  service.Session{},
				ID:       common.NewID("cred").String(),
				Name:     "cred1",
				Username: common.NewID("user").String(),
				Type:     "generic",
				Value:    strings.Repeat("a", i),
			}
			err := ps.Create(creationReq)
			if len(creationReq.Value) < 16000 {
				// we should be able to store 16K plaintext even if we encrypt at rest
				if !assert.NoErrorf(t, err, "len, %d", len(creationReq.Value)) {
					return
				}
			} else if len(creationReq.Value) > 16500 {
				// encrypted value should still trigger length limit (~16K)
				assert.Error(t, err)
				return
			}
			if err != nil {
				fmt.Printf("len: %d\n", i)
			}
		}
	})
}

func stringPtr(s string) *string {
	return &s
}

// openpgpEncrypt implements the default options from pgcrypto, the idea is that we want to be able to reproduce what postgres does in golang.
// https://www.postgresql.org/docs/12/pgcrypto.html#id-1.11.7.34.7
//
// Note: golang.org/x/crypto/openpgp is deprecated, so this is only implemented for testing purpose.
func openpgpEncrypt(plaintext []byte, psw []byte) ([]byte, error) {
	ciphertext := new(bytes.Buffer)
	plaintextWriter, err := openpgp.SymmetricallyEncrypt(ciphertext, psw, nil, &packet.Config{
		DefaultCipher:          packet.CipherAES256,
		DefaultCompressionAlgo: packet.CompressionNone,
		DefaultHash:            crypto.SHA1,
	})
	if err != nil {
		return nil, err
	}
	n, err := plaintextWriter.Write(plaintext)
	if err != nil {
		return nil, err
	}
	if n != len(plaintext) {
		panic(n)
	}
	plaintextWriter.Close()
	return ciphertext.Bytes(), nil
}

// see openpgpEncrypt
func openpgpDecrypt(ciphertext []byte, psw []byte) ([]byte, error) {
	var plaintext bytes.Buffer

	md, err := openpgp.ReadMessage(
		bytes.NewReader(ciphertext),
		nil,
		func(keys []openpgp.Key, symmetric bool) ([]byte, error) {
			return psw, nil
		},
		nil,
	)
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(&plaintext, md.UnverifiedBody)
	if err != nil {
		return nil, err
	}
	return plaintext.Bytes(), nil
}

// Test to make sure the golang encryption/decryption function are working with each other.
func TestPGP(t *testing.T) {
	plaintext := make([]byte, 100)
	_, err := rand.Read(plaintext)
	if err != nil {
		panic(err)
	}
	passphrase := make([]byte, 32)
	_, err = rand.Read(passphrase)
	if err != nil {
		panic(err)
	}
	ciphertext, err := openpgpEncrypt(plaintext, passphrase)
	if !assert.NoError(t, err) {
		return
	}
	decrypted, err := openpgpDecrypt(ciphertext, passphrase)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, plaintext, decrypted)
}
