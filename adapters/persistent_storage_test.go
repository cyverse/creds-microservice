package adapters

import (
	"fmt"
	"github.com/rs/xid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/adapters/storageschema"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"reflect"
	"strconv"
	"sync"
	"testing"
	"time"
)

// storageCreator should create a new client with a clean storage with no residue data
func testPersistentStorage(t *testing.T, storageCreator func(timeSrc ports.TimeSrc) ports.PersistentStoragePort) {
	t.Run("create & get", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })
		// Create
		credCreateRequest := service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar",
			Name:              "foobar",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "{}",
			Description:       "desc123",
			IsSystem:          true,
			IsHidden:          false,
			Disabled:          true,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         time.Time{}, // Note: this field should be overridden by the storage adapter
			UpdatedAt:         time.Time{}, // Note: this field should be overridden by the storage adapter
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err := storage.Create(credCreateRequest)
		assert.NoError(t, err)

		// Get
		credFetched, err := storage.Get(service.CredentialModel{
			Username: "testuser-123",
			ID:       "foobar",
		})
		assert.NoError(t, err)
		assert.Equal(t, now.Unix(), credFetched.CreatedAt.Unix())
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar",
			Name:              "foobar",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "{}",
			Description:       "desc123",
			IsSystem:          true,
			IsHidden:          false,
			Disabled:          false, // always create as enabled
			Visibility:        "",
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, credFetched)
	})
	t.Run("create w/ tags & get & list", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })
		// Create
		credCreateRequest := service.CredentialModel{
			Session:     service.Session{},
			ID:          "foobar",
			Name:        "foobar",
			Username:    "testuser-123",
			Type:        "type123",
			Value:       "{}",
			Description: "",
			IsSystem:    false,
			IsHidden:    false,
			Visibility:  "",
			Tags: map[string]string{
				"tag1": "val1",
				"tag2": "",
			},
			CreatedAt:         time.Time{}, // Note: this field should be overridden by the storage adapter
			UpdatedAt:         time.Time{}, // Note: this field should be overridden by the storage adapter
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err := storage.Create(credCreateRequest)
		assert.NoError(t, err)

		// Get
		credFetched, err := storage.Get(service.CredentialModel{
			Username: "testuser-123",
			ID:       "foobar",
		})
		assert.NoError(t, err)
		assert.Equal(t, now.Unix(), credFetched.CreatedAt.Unix())
		assert.Equal(t, service.CredentialModel{
			Session:     service.Session{},
			ID:          "foobar",
			Name:        "foobar",
			Username:    "testuser-123",
			Type:        "type123",
			Value:       "{}",
			Description: "",
			IsSystem:    false,
			IsHidden:    false,
			Visibility:  "",
			Tags: map[string]string{
				"tag1": "val1",
				"tag2": "",
			},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, credFetched)

		// List
		list, err := storage.List(types.ListQueryData{
			Actor:    "testuser-123",
			Emulator: "",
			Username: "testuser-123",
		})
		assert.NoError(t, err)
		assert.Len(t, list, 1)
		assert.Equal(t, service.CredentialModel{
			Session:     service.Session{},
			ID:          "foobar",
			Name:        "foobar",
			Username:    "testuser-123",
			Type:        "type123",
			Value:       "REDACTED",
			Description: "",
			IsSystem:    false,
			IsHidden:    false,
			Visibility:  "",
			Tags: map[string]string{
				"tag1": "val1",
				"tag2": "",
			},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, list[0])
	})
	t.Run("list filters", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })
		credBase := service.CredentialModel{
			Session:     service.Session{},
			ID:          "foobar",
			Name:        "",
			Username:    "testuser-123",
			Type:        "type123",
			Value:       "{}",
			Description: "",
			IsSystem:    false,
			IsHidden:    false,
			Visibility:  "",
			Tags: map[string]string{
				"tag1": "val1",
				"tag2": "",
			},
			CreatedAt:         time.Time{}, // Note: this field should be overridden by the storage adapter
			UpdatedAt:         time.Time{}, // Note: this field should be overridden by the storage adapter
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		// Create
		credCreateRequest := credBase
		credCreateRequest.ID = xid.New().String()
		credCreateRequest.Name = "type123-system-enabled-tags"
		credCreateRequest.IsSystem = true
		err := storage.Create(credCreateRequest)
		assert.NoError(t, err)

		credCreateRequest = credBase
		credCreateRequest.ID = xid.New().String()
		credCreateRequest.Name = "type123-nonsystem-enabled-tags"
		err = storage.Create(credCreateRequest)
		assert.NoError(t, err)

		credCreateRequest = credBase
		credCreateRequest.ID = xid.New().String()
		credCreateRequest.Name = "type123-system-enabled-notags"
		credCreateRequest.IsSystem = true
		credCreateRequest.Tags = map[string]string{}
		err = storage.Create(credCreateRequest)
		assert.NoError(t, err)

		credCreateRequest = credBase
		credCreateRequest.ID = xid.New().String()
		credCreateRequest.Name = "type123-nonsystem-enabled-notags"
		credCreateRequest.Tags = map[string]string{}
		err = storage.Create(credCreateRequest)
		assert.NoError(t, err)

		credCreateRequest = credBase
		credCreateRequest.ID = xid.New().String()
		credCreateRequest.Name = "type456-nonsystem-enabled-notags"
		credCreateRequest.Type = "type456"
		credCreateRequest.Tags = map[string]string{}
		err = storage.Create(credCreateRequest)
		assert.NoError(t, err)

		// List by type
		var nameList []string
		list, err := storage.List(types.ListQueryData{
			Actor:      "testuser-123",
			Emulator:   "",
			Username:   "testuser-123",
			Type:       "type123",
			IsSystem:   nil,
			Disabled:   nil,
			Visibility: "",
			Tags:       nil,
		})
		assert.NoError(t, err)
		assert.Len(t, list, 4)
		for _, cred := range list {
			nameList = append(nameList, cred.Name)
		}
		assert.Contains(t, nameList, "type123-system-enabled-tags")
		assert.Contains(t, nameList, "type123-nonsystem-enabled-tags")
		assert.Contains(t, nameList, "type123-system-enabled-notags")
		assert.Contains(t, nameList, "type123-nonsystem-enabled-notags")

		// List by system
		list, err = storage.List(types.ListQueryData{
			Actor:      "testuser-123",
			Emulator:   "",
			Username:   "testuser-123",
			Type:       "",
			IsSystem:   boolPtr(true),
			Disabled:   nil,
			Visibility: "",
			Tags:       nil,
		})
		assert.NoError(t, err)
		assert.Len(t, list, 2)
		nameList = make([]string, 0)
		for _, cred := range list {
			nameList = append(nameList, cred.Name)
		}
		assert.Contains(t, nameList, "type123-system-enabled-tags")
		assert.Contains(t, nameList, "type123-system-enabled-notags")

		// List by non-system
		list, err = storage.List(types.ListQueryData{
			Actor:      "testuser-123",
			Emulator:   "",
			Username:   "testuser-123",
			Type:       "",
			IsSystem:   boolPtr(false),
			Disabled:   nil,
			Visibility: "",
			Tags:       nil,
		})
		assert.NoError(t, err)
		assert.Len(t, list, 3)
		nameList = make([]string, 0)
		for _, cred := range list {
			nameList = append(nameList, cred.Name)
		}
		assert.Contains(t, nameList, "type123-nonsystem-enabled-tags")
		assert.Contains(t, nameList, "type123-nonsystem-enabled-notags")
		assert.Contains(t, nameList, "type456-nonsystem-enabled-notags")

		// List by tag name
		list, err = storage.List(types.ListQueryData{
			Actor:      "testuser-123",
			Emulator:   "",
			Username:   "testuser-123",
			Type:       "",
			IsSystem:   nil,
			Disabled:   nil,
			Visibility: "",
			Tags: map[string]string{
				"tag1": "",
			},
		})
		assert.NoError(t, err)
		assert.Len(t, list, 2)
		for _, cred := range list {
			nameList = append(nameList, cred.Name)
		}
		assert.Contains(t, nameList, "type123-system-enabled-tags")
		assert.Contains(t, nameList, "type123-nonsystem-enabled-tags")

		// List by tag name, not present
		list, err = storage.List(types.ListQueryData{
			Actor:      "testuser-123",
			Emulator:   "",
			Username:   "testuser-123",
			Type:       "",
			IsSystem:   nil,
			Disabled:   nil,
			Visibility: "",
			Tags: map[string]string{
				"not-exist": "",
			},
		})
		assert.NoError(t, err)
		assert.Len(t, list, 0)

		// List by tag value, present
		list, err = storage.List(types.ListQueryData{
			Actor:      "testuser-123",
			Emulator:   "",
			Username:   "testuser-123",
			Type:       "",
			IsSystem:   nil,
			Disabled:   nil,
			Visibility: "",
			Tags: map[string]string{
				"tag1": "val1",
			},
		})
		assert.NoError(t, err)
		assert.Len(t, list, 2)
		for _, cred := range list {
			nameList = append(nameList, cred.Name)
		}
		assert.Contains(t, nameList, "type123-system-enabled-tags")
		assert.Contains(t, nameList, "type123-nonsystem-enabled-tags")

		// List by tag value, not present
		list, err = storage.List(types.ListQueryData{
			Actor:      "testuser-123",
			Emulator:   "",
			Username:   "testuser-123",
			Type:       "",
			IsSystem:   nil,
			Disabled:   nil,
			Visibility: "",
			Tags: map[string]string{
				"tag1": "not-exist",
			},
		})
		assert.NoError(t, err)
		assert.Len(t, list, 0)
	})
	t.Run("create & get & delete & get", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })

		// Create
		credCreateRequest := service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar",
			Name:              "foobar",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "{}",
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err := storage.Create(credCreateRequest)
		assert.NoError(t, err)

		// Get
		credFetched, err := storage.Get(service.CredentialModel{
			Username: "testuser-123",
			ID:       "foobar",
		})
		assert.NoError(t, err)
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar",
			Name:              "foobar",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "{}",
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, credFetched)

		// Delete
		err = storage.Delete(service.CredentialModel{
			Username: "testuser-123",
			ID:       "foobar",
		})
		assert.NoError(t, err)

		// Get
		credFetched, err = storage.Get(service.CredentialModel{
			Username: "testuser-123",
			ID:       "foobar",
		})
		assert.Error(t, err)
		assert.Equal(t, service.CredentialModel{}, credFetched)
	})
	t.Run("re-create to overwrite", func(t *testing.T) {
		t.Skip("re-create not supported by postgres metadata storage") // FIXME
		//now := time.Now()
		//storage := storageCreator(func() time.Time { return now })
		//
		//// Create
		//credCreateRequest1 := service.CredentialModel{
		//	Session:           service.Session{},
		//	ID:                "foobar",
		//	Name:              "foobar",
		//	Username:          "testuser-123",
		//	Type:              "type123",
		//	Value:             "{}",
		//	Description:       "",
		//	IsSystem:          false,
		//	IsHidden:          false,
		//	Visibility:        "",
		//	Tags:              nil,
		//	CreatedAt:         time.Time{},
		//	UpdatedAt:         time.Time{},
		//	UpdatedBy:         "",
		//	UpdatedEmulatorBy: "",
		//}
		//err := storage.Create(credCreateRequest1)
		//assert.NoError(t, err)
		//
		//// Get
		//credFetched, err := storage.Get(service.CredentialModel{
		//	Username: "testuser-123",
		//	ID:       "foobar",
		//})
		//assert.NoError(t, err)
		//assert.Equal(t, service.CredentialModel{
		//	Session:           service.Session{},
		//	ID:                "foobar",
		//	Name:              "foobar",
		//	Username:          "testuser-123",
		//	Type:              "type123",
		//	Value:             "{}",
		//	Description:       "",
		//	IsSystem:          false,
		//	IsHidden:          false,
		//	Visibility:        "",
		//	Tags:              map[string]string{},
		//	CreatedAt:         storageschema.MilliSecPrecision(now),
		//	UpdatedAt:         storageschema.MilliSecPrecision(now),
		//	UpdatedBy:         "",
		//	UpdatedEmulatorBy: "",
		//}, credFetched)
		//
		//// Create again
		//credCreateRequest2 := service.CredentialModel{
		//	Session:           service.Session{},
		//	ID:                "foobar",
		//	Name:              "foobar",
		//	Username:          "testuser-123",
		//	Type:              "diff-type",
		//	Value:             "diff-value",
		//	Description:       "diff-description",
		//	IsSystem:          false,
		//	IsHidden:          false,
		//	Visibility:        "",
		//	Tags:              nil,
		//	CreatedAt:         now,
		//	UpdatedAt:         now,
		//	UpdatedBy:         "",
		//	UpdatedEmulatorBy: "",
		//}
		//err = storage.Create(credCreateRequest2)
		//assert.NoError(t, err)
		//
		//// Get
		//credFetched, err = storage.Get(service.CredentialModel{
		//	Username: "testuser-123",
		//	ID:       "foobar",
		//})
		//assert.NoError(t, err)
		//assert.Equal(t, service.CredentialModel{
		//	Session:           service.Session{},
		//	ID:                "foobar",
		//	Name:              "foobar",
		//	Username:          "testuser-123",
		//	Type:              "diff-type",
		//	Value:             "diff-value",
		//	Description:       "diff-description",
		//	IsSystem:          false,
		//	IsHidden:          false,
		//	Visibility:        "",
		//	Tags:              map[string]string{},
		//	CreatedAt:         storageschema.MilliSecPrecision(now),
		//	UpdatedAt:         storageschema.MilliSecPrecision(now),
		//	UpdatedBy:         "",
		//	UpdatedEmulatorBy: "",
		//}, credFetched)
		//assert.NotEqual(t, credCreateRequest1, credCreateRequest2)
	})
	t.Run("get non-existent", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })
		cred, err := storage.Get(service.CredentialModel{
			Session:  service.Session{},
			Username: "testuser-123",
			ID:       "not-exist",
		})
		assert.Error(t, err)
		err1, ok := err.(service.CacaoError)
		if assert.Truef(t, ok, reflect.TypeOf(err).String()) {
			assert.Equal(t, service.CacaoNotFoundErrorMessage, err1.StandardError())
		}
		assert.Equal(t, service.CredentialModel{}, cred)
	})
	t.Run("delete non-existent", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })
		_, err := storage.Get(service.CredentialModel{
			Session:  service.Session{},
			Username: "testuser-123",
			ID:       "not-exist",
		})
		assert.Error(t, err)

		err = storage.Delete(service.CredentialModel{
			Session:  service.Session{},
			Username: "testuser-123",
			ID:       "not-exist",
		})
		assert.NoError(t, err) // no error when delete credential that does not exists
	})
	t.Run("create & get w/ diff user", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })
		err := storage.Create(service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar",
			Name:              "foobar",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "value123",
			Description:       "diff-description",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              map[string]string{},
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		})
		if !assert.NoError(t, err) {
			return
		}
		cred, err := storage.Get(service.CredentialModel{
			Session:  service.Session{},
			Username: "diff-user-123", // diff username
			ID:       "foobar",        // same ID
		})
		assert.Error(t, err)
		assert.Equal(t, service.CredentialModel{}, cred)
	})
	t.Run("list empty", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })
		list, err := storage.List(types.ListQueryData{
			Actor:    "",
			Emulator: "",
			Username: "testuser-123",
		})
		assert.NoError(t, err)
		assert.Empty(t, list)
		assert.Nil(t, list) // expect nil instead of empty slice
	})
	t.Run("create 1 & list 1", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })

		// Create
		credCreateRequest := service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar",
			Name:              "foobar",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "{}",
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err := storage.Create(credCreateRequest)
		assert.NoError(t, err)

		// List
		list, err := storage.List(types.ListQueryData{
			Actor:    "",
			Emulator: "",
			Username: "testuser-123",
		})
		assert.NoError(t, err)
		assert.NotNil(t, list)
		assert.NotEmpty(t, list)
		if !assert.Len(t, list, 1) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar",
			Name:              "foobar",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "REDACTED",
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, list[0])
	})
	t.Run("create 1 & list w/ diff user", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })

		// Create
		credCreateRequest := service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar",
			Name:              "foobar",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "{}",
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err := storage.Create(credCreateRequest)
		assert.NoError(t, err)

		// List
		list, err := storage.List(types.ListQueryData{
			Actor:    "",
			Emulator: "",
			Username: "diff-user-123", // different user
		})
		assert.NoError(t, err)
		assert.Nil(t, list) // return nil
		assert.Empty(t, list)
	})
	testListN := func(t *testing.T, N int) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })

		// Create N
		for i := 0; i < N; i++ {
			credCreateRequest1 := service.CredentialModel{
				Session:           service.Session{},
				ID:                fmt.Sprintf("foobar-%d", i),
				Name:              fmt.Sprintf("foobar-%d", i),
				Username:          "testuser-123",
				Type:              service.CredentialType(fmt.Sprintf("type-%d", i)),
				Value:             fmt.Sprintf("val-%d", i),
				Description:       fmt.Sprintf("desc-%d", i),
				IsSystem:          false,
				IsHidden:          false,
				Visibility:        "",
				Tags:              nil,
				CreatedAt:         now,
				UpdatedAt:         now,
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			}
			err := storage.Create(credCreateRequest1)
			assert.NoError(t, err)
		}

		// List
		list, err := storage.List(types.ListQueryData{
			Actor:    "",
			Emulator: "",
			Username: "testuser-123",
		})
		assert.NoError(t, err)
		assert.NotNil(t, list)
		assert.NotEmpty(t, list)
		if !assert.Len(t, list, N) {
			return
		}

		// Check
		var idSet = make(map[string]bool) // used to check if IDs are expected, and no expected ID
		for i := 0; i < N; i++ {
			idSet[fmt.Sprintf("foobar-%d", i)] = false
		}
		for i := 0; i < N; i++ {
			idSet[list[i].ID] = true
			indexFromID, err := strconv.ParseInt(list[i].ID[7:], 10, 32) // e.g. extract '7' from 'foobar-7'
			assert.NoError(t, err)
			assert.Equal(t, service.CredentialModel{
				Session:           service.Session{},
				ID:                list[i].ID,
				Name:              list[i].Name,
				Username:          "testuser-123",
				Type:              service.CredentialType(fmt.Sprintf("type-%d", indexFromID)),
				Value:             "REDACTED",
				Description:       fmt.Sprintf("desc-%d", indexFromID),
				IsSystem:          false,
				IsHidden:          false,
				Visibility:        "",
				Tags:              map[string]string{},
				CreatedAt:         storageschema.MilliSecPrecision(now),
				UpdatedAt:         storageschema.MilliSecPrecision(now),
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			}, list[i])
		}
		// check that all expected ID are present
		for credID, idExists := range idSet {
			assert.Truef(t, idExists, "id '%s' should be set", credID)
		}
	}
	t.Run("create N & list N", func(t *testing.T) {
		testListN(t, 10)
	})
	t.Run("create N & list N (large N)", func(t *testing.T) {
		testListN(t, 1000)
	})
	t.Run("create 3 & delete 1 & list 2", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })

		// Create 3
		credCreateRequest1 := service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar-1",
			Name:              "foobar-1",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             `{"k1":"v1"}`,
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         now,
			UpdatedAt:         now,
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err := storage.Create(credCreateRequest1)
		assert.NoError(t, err)

		credCreateRequest2 := service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar-2",
			Name:              "foobar-2",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             `{"k2":"v2"}`,
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         now,
			UpdatedAt:         now,
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err = storage.Create(credCreateRequest2)
		assert.NoError(t, err)

		credCreateRequest3 := service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar-3",
			Name:              "foobar-3",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             `{"k3":"v3"}`,
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         time.Time{},
			UpdatedAt:         time.Time{},
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err = storage.Create(credCreateRequest3)
		assert.NoError(t, err)

		// Delete 1
		err = storage.Delete(service.CredentialModel{
			Username: "testuser-123",
			ID:       "foobar-2",
		})
		assert.NoError(t, err)

		// List 2
		list, err := storage.List(types.ListQueryData{
			Actor:    "",
			Emulator: "",
			Username: "testuser-123",
		})
		assert.NoError(t, err)
		assert.NotNil(t, list)
		assert.NotEmpty(t, list)
		if !assert.Len(t, list, 2) {
			return
		}
		for _, listItem := range list {
			assert.Contains(t, []string{"foobar-1", "foobar-3"}, listItem.ID)
			assert.Contains(t, []string{"foobar-1", "foobar-3"}, listItem.Name)
			assert.Equal(t, service.CredentialModel{
				Session:           service.Session{},
				ID:                listItem.ID,
				Name:              listItem.Name,
				Username:          "testuser-123",
				Type:              "type123",
				Value:             "REDACTED",
				Description:       "",
				IsSystem:          false,
				IsHidden:          false,
				Visibility:        "",
				Tags:              map[string]string{},
				CreatedAt:         storageschema.MilliSecPrecision(now),
				UpdatedAt:         storageschema.MilliSecPrecision(now),
				UpdatedBy:         "",
				UpdatedEmulatorBy: "",
			}, listItem)
		}
	})

}

// split out some tests as extra, these tests are not run on vault adapter.
// once vault adapter is removed or reworked, merge this back with testPersistentStorage().
func testPersistentStorageExtra(t *testing.T, storageCreator func(timeSrc ports.TimeSrc) ports.PersistentStoragePort) {
	t.Run("concurrent create N & list N", func(t *testing.T) {
		// spawn 2 go routines, one creates N credential, another performs N list operations.
		const N = 1000
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })

		var idSet = make(map[string]bool) // used to check if IDs are expected, and no expected ID
		for i := 0; i < N; i++ {
			idSet[fmt.Sprintf("foobar-%d", i)] = false
		}

		var wg sync.WaitGroup
		wg.Add(2)

		// Create N
		go func() {
			defer wg.Done()
			for i := 0; i < N; i++ {
				credCreateRequest1 := service.CredentialModel{
					Session:           service.Session{},
					ID:                fmt.Sprintf("foobar-%d", i),
					Name:              fmt.Sprintf("foobar-%d", i),
					Username:          "testuser-123",
					Type:              service.CredentialType(fmt.Sprintf("type-%d", i)),
					Value:             fmt.Sprintf("val-%d", i),
					Description:       fmt.Sprintf("desc-%d", i),
					IsSystem:          false,
					IsHidden:          false,
					Visibility:        "",
					Tags:              nil,
					CreatedAt:         now,
					UpdatedAt:         now,
					UpdatedBy:         "",
					UpdatedEmulatorBy: "",
				}
				err := storage.Create(credCreateRequest1)
				assert.NoError(t, err)
			}
		}()

		go func() {
			defer wg.Done()

			const numberOfListOperations = N
			for j := 0; j < numberOfListOperations; j++ {
				list, err := storage.List(types.ListQueryData{
					Actor:    "",
					Emulator: "",
					Username: "testuser-123",
				})
				if !assert.NoError(t, err) {
					return
				}
				assert.GreaterOrEqual(t, len(list), 0)
				assert.LessOrEqual(t, len(list), N)

				// Check for each cred in fetched list
				for i := 0; i < len(list); i++ {
					indexFromID, err := strconv.ParseInt(list[i].ID[7:], 10, 32) // e.g. extract '7' from 'foobar-7'
					assert.NoError(t, err)
					assert.Equal(t, service.CredentialModel{
						Session:           service.Session{},
						ID:                list[i].ID,
						Name:              list[i].Name,
						Username:          "testuser-123",
						Type:              service.CredentialType(fmt.Sprintf("type-%d", indexFromID)),
						Value:             "REDACTED",
						Description:       fmt.Sprintf("desc-%d", indexFromID),
						IsSystem:          false,
						IsHidden:          false,
						Visibility:        "",
						Tags:              map[string]string{},
						CreatedAt:         storageschema.MilliSecPrecision(now),
						UpdatedAt:         storageschema.MilliSecPrecision(now),
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
					}, list[i])
				}
			}
		}()

		wg.Wait()
	})
	t.Run("create & update name & get", func(t *testing.T) {
		now := time.Now()
		storage := storageCreator(func() time.Time { return now })

		// Create
		credCreateRequest := service.CredentialModel{
			Session:           service.Session{},
			ID:                "foobar-1",
			Name:              "foobar-1",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             `{"k1":"v1"}`,
			Description:       "",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              nil,
			CreatedAt:         now,
			UpdatedAt:         now,
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}
		err := storage.Create(credCreateRequest)
		if !assert.NoError(t, err) {
			return
		}

		// Update name
		err = storage.Update(types.CredUpdate{
			SessionActor:    "testuser-123",
			SessionEmulator: "",
			Username:        "testuser-123",
			ID:              "foobar-1",
			Update: service.CredentialUpdate{
				Name:            stringPtr("new-name-123"),
				Value:           nil,
				Description:     nil,
				Disabled:        nil,
				Visibility:      nil,
				UpdateOrAddTags: nil,
				DeleteTags:      nil,
			},
		})
		assert.NoError(t, err)

		// Get
		fetched, err := storage.Get(service.CredentialModel{
			ID:       "foobar-1",
			Username: "testuser-123",
		})
		assert.NoError(t, err)
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                credCreateRequest.ID,
			Name:              "new-name-123",
			Username:          credCreateRequest.Username,
			Type:              credCreateRequest.Type,
			Value:             credCreateRequest.Value,
			Description:       credCreateRequest.Description,
			IsSystem:          credCreateRequest.IsSystem,
			IsHidden:          credCreateRequest.IsHidden,
			Visibility:        credCreateRequest.Visibility,
			Tags:              map[string]string{},
			CreatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedAt:         storageschema.MilliSecPrecision(now),
			UpdatedBy:         "testuser-123",
			UpdatedEmulatorBy: "",
		}, fetched)
	})
}

func boolPtr(b bool) *bool {
	return &b
}
