package adapters

import (
	vault "github.com/hashicorp/vault/api"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/adapters/storageschema"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"os"
	"testing"
	"time"
)

func TestPingVault(t *testing.T) {
	t.Run("not vault api", func(t *testing.T) {
		t.Parallel()
		cfg := types.Config{
			VaultAddress: "https://gitlab.com",
			VaultToken:   "whatever",
		}
		vaultClient, err := vaultClientFromConfig(cfg)
		if err != nil {
			panic(err)
		}
		err = pingVault(vaultClient)
		assert.Error(t, err)
	})

	t.Run("port not open", func(t *testing.T) {
		t.Parallel()
		cfg := types.Config{
			VaultAddress: "https://cyverse.org:1234",
			VaultToken:   "whatever",
		}
		vaultClient, err := vaultClientFromConfig(cfg)
		if err != nil {
			panic(err)
		}
		err = pingVault(vaultClient)
		assert.Error(t, err)
	})

	t.Run("test against vault (integration test)", func(t *testing.T) {
		if skipIfVaultEnvNotSet(t) {
			return
		}
		cfg := types.Config{
			VaultAddress: os.Getenv("VAULT_ADDR"),
			VaultToken:   os.Getenv("VAULT_TOKEN"),
		}
		vaultClient, err := vaultClientFromConfig(cfg)
		if err != nil {
			// fail to create vault client
			panic(err)
		}
		err = pingVault(vaultClient)
		assert.NoError(t, err)
	})
}

func TestPersistentStorageVault(t *testing.T) {
	if skipIfVaultEnvNotSet(t) {
		return
	}
	testPersistentStorage(t, func(timeSrc ports.TimeSrc) ports.PersistentStoragePort {
		cfg := types.Config{
			VaultAddress: os.Getenv("VAULT_ADDR"),
			VaultToken:   os.Getenv("VAULT_TOKEN"),
		}
		client, err := NewVaultCredentialStorage(cfg, timeSrc)
		if err != nil {
			t.Fatalf("fail to create vault adapter, %v. Check VAULT_ADDR and VAULT_TOKEN env var.", err)
		}
		vaultClient, err := vaultClientFromConfig(cfg)
		if err != nil {
			// fail to create vault client
			panic(err)
		}
		vaultCleanup(vaultClient)
		return client
	})
}

// this tests whether the storage adapter can read existing data in the vault that might have different schema.
func TestVaultStorageExitingData(t *testing.T) {
	if skipIfVaultEnvNotSet(t) {
		return
	}
	cfg := types.Config{
		VaultAddress: os.Getenv("VAULT_ADDR"),
		VaultToken:   os.Getenv("VAULT_TOKEN"),
	}
	vaultClient, err := vaultClientFromConfig(cfg)
	if err != nil {
		// fail to create vault client
		panic(err)
	}
	t.Run("simple string cred", func(t *testing.T) {
		vaultCleanup(vaultClient)
		_, err = vaultClient.Logical().Write("secret/testuser-123/secrets/cred-123", map[string]interface{}{
			"Type":        "type123",
			"Value":       "val1",
			"Description": "description123",
			"IsSystem":    false,
			"IsHidden":    false,
			"Visibility":  "",
			"Tags":        "",
			"CreatedAt":   "1655310000000",
			"UpdatedAt":   "1655310000000",
		})
		if err != nil {
			panic(err)
		}

		storage, err := NewVaultCredentialStorage(cfg, func() time.Time {
			panic("fetching credential should not need current time")
		})
		if err != nil {
			t.Fatalf("fail to create vault adapter, %v. Check VAULT_ADDR and VAULT_TOKEN env var.", err)
		}
		fetched, err := storage.Get(service.CredentialModel{
			Username: "testuser-123",
			ID:       "cred-123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                "cred-123",
			Name:              "cred-123",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "val1",
			Description:       "description123",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "",
			Tags:              map[string]string{},
			CreatedAt:         storageschema.StringMsToTime("1655310000000"),
			UpdatedAt:         storageschema.StringMsToTime("1655310000000"),
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, fetched)
	})
	t.Run("default visibility", func(t *testing.T) {
		vaultCleanup(vaultClient)
		_, err = vaultClient.Logical().Write("secret/testuser-123/secrets/cred-123", map[string]interface{}{
			"Type":        "type123",
			"Value":       "val1",
			"Description": "description123",
			"IsSystem":    false,
			"IsHidden":    false,
			"Visibility":  "Default",
			"Tags":        "tag1:~`;;-~!!:tag2",
			"CreatedAt":   "1655310000000",
			"UpdatedAt":   "1655310000000",
		})
		if err != nil {
			panic(err)
		}

		storage, err := NewVaultCredentialStorage(cfg, func() time.Time {
			panic("fetching credential should not need current time")
		})
		if err != nil {
			t.Fatalf("fail to create vault adapter, %v. Check VAULT_ADDR and VAULT_TOKEN env var.", err)
		}
		fetched, err := storage.Get(service.CredentialModel{
			Username: "testuser-123",
			ID:       "cred-123",
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.Equal(t, service.CredentialModel{
			Session:           service.Session{},
			ID:                "cred-123",
			Name:              "cred-123",
			Username:          "testuser-123",
			Type:              "type123",
			Value:             "val1",
			Description:       "description123",
			IsSystem:          false,
			IsHidden:          false,
			Visibility:        "Default",
			Tags:              map[string]string{"tag1": "", "tag2": ""},
			CreatedAt:         storageschema.StringMsToTime("1655310000000"),
			UpdatedAt:         storageschema.StringMsToTime("1655310000000"),
			UpdatedBy:         "",
			UpdatedEmulatorBy: "",
		}, fetched)
	})
}

// --------------------------------------------------------------------------------
// Warning: this will delete all data in the vault (if access permitted), ONLY used for integration test
// --------------------------------------------------------------------------------
func vaultCleanup(client *vault.Client) {
	// disable and then enable kv secret engine to clear the data
	err := client.Sys().Unmount(vaultNamespace)
	if err != nil {
		panic(err)
	}
	err = client.Sys().Mount(vaultNamespace, &vault.MountInput{
		Type: "kv",
		Options: map[string]string{
			"version": "1",
		},
	})
	if err != nil {
		panic(err)
	}
}

func Test_vaultAPI_extractIDListFromSecret(t *testing.T) {
	type fields struct {
		client *vault.Logical
	}
	type args struct {
		list *vault.Secret
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		{
			name:   "normal list",
			fields: fields{},
			args: args{
				list: &vault.Secret{
					Data: map[string]interface{}{
						"keys": []interface{}{
							"cred-id-1",
							"cred-id-2",
							"cred-id-3",
						},
					},
				},
			},
			want:    []string{"cred-id-1", "cred-id-2", "cred-id-3"},
			wantErr: false,
		},
		{
			name:   "data is nil",
			fields: fields{},
			args: args{
				list: &vault.Secret{
					Data: nil,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name:   "data is empty map",
			fields: fields{},
			args: args{
				list: &vault.Secret{
					Data: map[string]interface{}{},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name:   "data has no 'keys'",
			fields: fields{},
			args: args{
				list: &vault.Secret{
					Data: map[string]interface{}{
						"not-keys": []interface{}{
							"cred-id-1",
							"cred-id-2",
							"cred-id-3",
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name:   "value of 'keys' is nil",
			fields: fields{},
			args: args{
				list: &vault.Secret{
					Data: map[string]interface{}{
						"keys": nil,
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name:   "value of 'keys' has non-string",
			fields: fields{},
			args: args{
				list: &vault.Secret{
					Data: map[string]interface{}{
						"keys": []interface{}{
							nil,
							123,
							456,
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name:   "value of 'keys' contains string & non-string",
			fields: fields{},
			args: args{
				list: &vault.Secret{
					Data: map[string]interface{}{
						"keys": []interface{}{
							"cred-id-1",
							123,
						},
					},
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vapi := vaultAPI{
				client: tt.fields.client,
			}
			got, err := vapi.extractIDListFromSecret(tt.args.list)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equalf(t, tt.want, got, "extractIDListFromSecretData(%v)", tt.args.list)
		})
	}
}
