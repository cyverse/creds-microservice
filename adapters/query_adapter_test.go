package adapters

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/creds-microservice/ports/mocks"
	"gitlab.com/cyverse/creds-microservice/types"
	"sync"
	"testing"
	"time"
)

func TestNewNATSQueryAdapter(t *testing.T) {
	if skipIfSTANEnvNotSet(t) {
		t.Skip("CI_INTEGRATION_STAN is not 'true', skipping test")
		return
	}
	var conf types.Config
	err := envconfig.Process("", &conf)
	if !assert.NoError(t, err) {
		return
	}
	conf.ProcessDefaults()
	conf.Override()

	qa := &QueryAdapter{}
	defer func() {
		err := qa.Close()
		if !assert.NoError(t, err) {
			return
		}
	}()

	handlers := &portsmocks.IncomingQueryHandlers{}
	handlers.On("CredentialsGet", mock.Anything, service.CredentialGetRequest{
		Session: service.Session{
			SessionActor: "testuser123",
		},
		Username: "testuser123",
		ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
	}).Return(service.CredentialGetReply{
		Session: service.Session{
			SessionActor: "testuser123",
		},
		ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
		Name:     "name123",
		Username: "testuser123",
		Type:     "generic",
		Value:    "value123",
	}).Once()
	qa.SetHandlers(handlers)

	err = qa.Init(conf)
	if !assert.NoError(t, err) {
		return
	}
	ctx, cancelFunc := context.WithTimeout(context.Background(), time.Minute*5)
	defer cancelFunc()
	var wg sync.WaitGroup
	wg.Add(1)
	err = qa.Start(ctx, &wg)
	if !assert.NoError(t, err) {
		return
	}
	time.Sleep(time.Second)
	requestCe, err := messaging2.CreateCloudEvent(service.CredentialGetRequest{
		Session: service.Session{
			SessionActor: "testuser123",
		},
		ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
		Username: "testuser123",
	}, service.NatsSubjectCredentialsGet, messaging2.AutoPopulateCloudEventSource)
	if !assert.NoError(t, err) {
		return
	}
	replyCe, err := qa.nc.Request(ctx, requestCe)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, service.NatsSubjectCredentialsGet+types.QueryReplyTypePostfix, common.QueryOp(replyCe.Type()))
	cancelFunc()
	wg.Wait()
}
