/*
 This file is embedded in a string variable in postgresql_adapter.go
 */

/* credential_schema_version is used to store the schema version of the credential data, maximum number is the current
   version, this helps to know whether to run migration or not. */
CREATE TABLE IF NOT EXISTS credential_schema_version (version INT PRIMARY KEY, inserted_at TIMESTAMP DEFAULT NOW());
INSERT INTO credential_schema_version (version) VALUES (1) ON CONFLICT DO NOTHING;

/* enable pgcrypto extension for encrypting credential value in postgres */
CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE IF NOT EXISTS credential
(
    id                  VARCHAR(30) PRIMARY KEY CHECK (char_length(id) > 0),
    name                VARCHAR(100) NOT NULL CHECK (char_length(name) > 0),
    username            VARCHAR(100) NOT NULL CHECK (char_length(username) > 0),
    type                VARCHAR(100) NOT NULL CHECK (char_length(type) > 0),
    description         VARCHAR(500) NOT NULL,
    value               BYTEA        NOT NULL CHECK (octet_length(value) < 16384), -- this is encrypted now
    is_system           BOOLEAN      NOT NULL DEFAULT FALSE,
    visibility          VARCHAR(50)  NOT NULL,               -- not used currently
    disabled            BOOLEAN      NOT NULL DEFAULT FALSE, -- whether the credential is disabled, if a credential is disabled it should not be used (other services needs to respect that).
    created_at          TIMESTAMP    NOT NULL,
    updated_at          TIMESTAMP    NOT NULL,
    created_emulator_by VARCHAR(100) NOT NULL,
    updated_by          VARCHAR(100) NOT NULL,
    updated_emulator_by VARCHAR(100) NOT NULL,
    UNIQUE (username, name)                                  -- a user cannot have multiple credential with the same name
);

CREATE TABLE IF NOT EXISTS credential_tags
(
    id        VARCHAR(30)  NOT NULL,
    tag_name  VARCHAR(200) NOT NULL CHECK (char_length(tag_name) > 0),
    tag_value VARCHAR(256) NULL,
    FOREIGN KEY (id) REFERENCES credential (id),
    UNIQUE (id, tag_name) -- no duplicated tag name for a given credential
);

CREATE INDEX IF NOT EXISTS credential_index_username ON credential (username);
CREATE INDEX IF NOT EXISTS credential_index_type ON credential (type);
CREATE INDEX IF NOT EXISTS credential_tags_index_id ON credential_tags (id);

