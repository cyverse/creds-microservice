/* migrate v1 to v2 (credential value encryption) */
CREATE TABLE IF NOT EXISTS credential_v1_to_v2
(
    id       VARCHAR(30) PRIMARY KEY,
    migrated BOOLEAN NOT NULL DEFAULT FALSE
);
