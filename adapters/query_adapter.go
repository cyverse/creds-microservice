package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"sync"
)

// QueryAdapter is an example of a Driver Adapter.
type QueryAdapter struct {
	config   types.Config
	handlers ports.IncomingQueryHandlers

	nc *messaging2.NatsConnection
}

// NewNATSQueryAdapter ...
func NewNATSQueryAdapter() *QueryAdapter {
	return &QueryAdapter{}
}

// Init is the init function required per the Port interface
func (q *QueryAdapter) Init(c types.Config) error {
	log.Debug("starting adapters.QueryAdapter.Init()")
	q.config = c
	log.WithFields(log.Fields{
		"NatsURL":     q.config.Messaging.URL,
		"NatsSubject": q.config.Messaging.WildcardSubject,
	}).Info()
	nc, err := q.config.Messaging.ConnectNats()
	if err != nil {
		log.WithError(err).Error("fail to connect to NATS")
		return err
	}
	q.nc = &nc
	return nil
}

// SetHandlers ...
func (q *QueryAdapter) SetHandlers(handlers ports.IncomingQueryHandlers) {
	q.handlers = handlers
}

// Start tells the adapter to start listening for queries. The adapter will stop
// listening if context.Context is canceled. sync.WaitGroup.Done() will be called
// when adapter stopped listening.
func (q *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "QueryAdapter.Start",
	})
	logger.Debug("starting adapter")
	err := q.nc.Listen(ctx, map[common.QueryOp]messaging2.QueryHandlerFunc{
		service.NatsSubjectCredentialsGet: func(ctx2 context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
			var request service.CredentialGetRequest
			err := json.Unmarshal(requestCe.Data(), &request)
			if err != nil {
				return
			}
			reply := q.handlers.CredentialsGet(ctx2, request)
			replyCe, err := messaging2.CreateCloudEvent(reply, common.QueryOp(requestCe.Type()+types.QueryReplyTypePostfix), messaging2.AutoPopulateCloudEventSource)
			if err != nil {
				return
			}
			err = writer.Write(replyCe)
			if err != nil {
				return
			}
		},
		service.NatsSubjectCredentialsList: func(ctx2 context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
			var request service.CredentialListRequest
			err := json.Unmarshal(requestCe.Data(), &request)
			if err != nil {
				return
			}
			reply := q.handlers.CredentialsList(ctx2, request)
			// NOTE we only send over the []service.CredentialModel, not the entire reply, this means client does not know the error
			replyCe, err := messaging2.CreateCloudEvent(reply.List, common.QueryOp(requestCe.Type()+types.QueryReplyTypePostfix), messaging2.AutoPopulateCloudEventSource)
			if err != nil {
				return
			}
			err = writer.Write(replyCe)
			if err != nil {
				return
			}
		},
	}, wg)
	if err != nil {
		log.WithError(err).Error()
		return err
	}
	return nil
}

// Close closes the connection
func (q *QueryAdapter) Close() error {
	log.Info("closing NATS connection for query adapter")
	if q.nc == nil {
		return nil
	}
	return q.nc.Close()
}
