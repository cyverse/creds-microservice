package adapters

import (
	"context"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"sync"
)

// StanEventAdapter ...
type StanEventAdapter struct {
	conn     *messaging2.StanConnection
	handlers ports.IncomingEventHandlers
}

var _ ports.IncomingEventPort = &StanEventAdapter{}

// Init ...
func (sa *StanEventAdapter) Init(conf types.Config) error {
	stanConn, err := conf.Messaging.ConnectStan()
	if err != nil {
		log.WithError(err).Error("fail to init stan conn")
		return err
	}
	sa.conn = &stanConn
	return nil
}

// SetHandlers ...
func (sa *StanEventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	sa.handlers = handlers
}

// Start ...
func (sa *StanEventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	err := sa.conn.Listen(ctx, map[common.EventType]messaging2.EventHandlerFunc{
		service.EventCredentialAddRequested:        handlerWrapper(sa.handlers.EventCredentialAddRequested),
		service.EventCredentialDeleteRequested:     handlerWrapper(sa.handlers.EventCredentialDeleteRequested),
		service.EventCredentialUpdateRequested:     handlerWrapper(sa.handlers.EventCredentialUpdateRequested),
		service.EventCredentialGenerationRequested: handlerWrapper(sa.handlers.EventSystemCredentialGenerationRequested),
	}, wg)
	if err != nil {
		log.WithError(err).Error("fail to subscribe for events on STAN")
		return err
	}
	return nil
}

// this is a type set of struct for incoming event requests
type eventRequestStructTypeSet interface {
	service.CredentialCreateRequest | service.CredentialDeleteRequest | service.CredentialUpdateRequest
}

func handlerWrapper[T eventRequestStructTypeSet](handler func(context.Context, T, ports.OutgoingEvents)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := event.DataAs(&request)
		if err != nil {
			return err
		}
		handler(ctx, request, EventOut{writer: writer})
		return nil
	}
}

// Close ...
func (sa *StanEventAdapter) Close() error {
	return sa.conn.Close()
}

// EventOut ...
type EventOut struct {
	writer messaging2.EventResponseWriter
}

var _ ports.OutgoingEvents = (*EventOut)(nil)

// EventCredentialAdded ...
func (e EventOut) EventCredentialAdded(response service.CredentialCreateResponse) {
	e.publish(response, service.EventCredentialAdded)
}

// EventCredentialAddError ...
func (e EventOut) EventCredentialAddError(response service.CredentialCreateResponse) {
	e.publish(response, service.EventCredentialAddError)
}

// EventCredentialDeleted ...
func (e EventOut) EventCredentialDeleted(response service.CredentialDeleteResponse) {
	e.publish(response, service.EventCredentialDeleted)
}

// EventCredentialDeleteError ...
func (e EventOut) EventCredentialDeleteError(response service.CredentialDeleteResponse) {
	e.publish(response, service.EventCredentialDeleteError)
}

// EventCredentialUpdated ...
func (e EventOut) EventCredentialUpdated(response service.CredentialUpdateResponse) {
	e.publish(response, service.EventCredentialUpdated)
}

// EventCredentialUpdateError ...
func (e EventOut) EventCredentialUpdateError(response service.CredentialUpdateResponse) {
	e.publish(response, service.EventCredentialUpdateError)
}

func (e EventOut) publish(response interface{}, eventType common.EventType) {
	ce, err := messaging2.CreateCloudEvent(response, eventType, messaging2.AutoPopulateCloudEventSource)
	if err != nil {
		return
	}
	err = e.writer.Write(ce)
	if err != nil {
		log.WithFields(log.Fields{
			"ceType": eventType,
		}).WithError(err).Error("fail to write cloudevent response")
		return
	}
}
