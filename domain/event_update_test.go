package domain

import (
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/creds-microservice/ports/mocks"
	"gitlab.com/cyverse/creds-microservice/types"
	"testing"
	"time"
)

func Test_handleUpdateEvent1(t *testing.T) {
	type args struct {
		request service.CredentialUpdateRequest
		storage *portsmocks.PersistentStoragePort
		sink    *portsmocks.OutgoingEvents
	}
	tests := []struct {
		name string
		args args
	}{

		{
			name: "empty request",
			args: args{
				request: service.CredentialUpdateRequest{},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialUpdateError", service.CredentialUpdateResponse{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ErrorType:       "invalid parameter error - username cannot be empty",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoInvalidParameterError("username cannot be empty").GetBase(),
						},
						Username:     "",
						ID:           "",
						Type:         "",
						ValueUpdated: false,
					}).Once()
					return sink
				}(),
			},
		},
		{
			name: "update name",
			args: args{
				request: service.CredentialUpdateRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
					ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
					Update: service.CredentialUpdate{
						Name: stringPtr("newName"),
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", service.CredentialModel{
						ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
						Username: "testuser123",
					}).Return(service.CredentialModel{
						Session:           service.Session{},
						ID:                "cred-aaaaaaaaaaaaaaaaaaaa",
						Name:              "oldName",
						Username:          "testuser123",
						Type:              "generic",
						Value:             "foobar",
						Description:       "",
						IsSystem:          false,
						IsHidden:          false,
						Disabled:          false,
						Visibility:        "",
						Tags:              nil,
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
					}, nil).Once()
					storage.On("Update", types.CredUpdate{
						SessionActor:    "testuser123",
						SessionEmulator: "",
						Username:        "testuser123",
						ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
						Update: service.CredentialUpdate{
							Name: stringPtr("newName"),
						},
					}).Return(nil).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialUpdated", service.CredentialUpdateResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Username:     "testuser123",
						ID:           "cred-aaaaaaaaaaaaaaaaaaaa",
						Type:         "generic",
						ValueUpdated: false,
					}).Once()
					return sink
				}(),
			},
		},
		{
			name: "disable",
			args: args{
				request: service.CredentialUpdateRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
					ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
					Update: service.CredentialUpdate{
						Disabled: boolPtr(true),
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", service.CredentialModel{
						ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
						Username: "testuser123",
					}).Return(service.CredentialModel{
						Session:           service.Session{},
						ID:                "cred-aaaaaaaaaaaaaaaaaaaa",
						Name:              "oldName",
						Username:          "testuser123",
						Type:              "generic",
						Value:             "foobar",
						Description:       "",
						IsSystem:          false,
						IsHidden:          false,
						Disabled:          false,
						Visibility:        "",
						Tags:              nil,
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
					}, nil).Once()
					storage.On("Update", types.CredUpdate{
						SessionActor:    "testuser123",
						SessionEmulator: "",
						Username:        "testuser123",
						ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
						Update: service.CredentialUpdate{
							Disabled: boolPtr(true),
						},
					}).Return(nil).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialUpdated", service.CredentialUpdateResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Username:     "testuser123",
						ID:           "cred-aaaaaaaaaaaaaaaaaaaa",
						Type:         "generic",
						ValueUpdated: false,
					}).Once()
					return sink
				}(),
			},
		},
		{
			name: "add tags",
			args: args{
				request: service.CredentialUpdateRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
					ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
					Update: service.CredentialUpdate{
						UpdateOrAddTags: map[string]string{
							"f1": "v1",
							"f2": "v2",
						},
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", service.CredentialModel{
						ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
						Username: "testuser123",
					}).Return(service.CredentialModel{
						Session:           service.Session{},
						ID:                "cred-aaaaaaaaaaaaaaaaaaaa",
						Name:              "oldName",
						Username:          "testuser123",
						Type:              "generic",
						Value:             "foobar",
						Description:       "",
						IsSystem:          false,
						IsHidden:          false,
						Disabled:          false,
						Visibility:        "",
						Tags:              nil,
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
					}, nil).Once()
					storage.On("Update", types.CredUpdate{
						SessionActor:    "testuser123",
						SessionEmulator: "",
						Username:        "testuser123",
						ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
						Update: service.CredentialUpdate{
							UpdateOrAddTags: map[string]string{
								"f1": "v1",
								"f2": "v2",
							},
						},
					}).Return(nil).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialUpdated", service.CredentialUpdateResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Username:     "testuser123",
						ID:           "cred-aaaaaaaaaaaaaaaaaaaa",
						Type:         "generic",
						ValueUpdated: false,
					}).Once()
					return sink
				}(),
			},
		},
		{
			name: "update multiple fields",
			args: args{
				request: service.CredentialUpdateRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "testuser123",
					ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
					Update: service.CredentialUpdate{
						Name:        stringPtr("newName"),
						Value:       stringPtr("newVal"),
						Description: stringPtr("newDesc"),
						Disabled:    boolPtr(true),
						Visibility:  (*service.VisibilityType)(stringPtr("Default")),
						UpdateOrAddTags: map[string]string{
							"f1": "v1",
						},
						DeleteTags: map[string]struct{}{
							"f2": {},
						},
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Get", service.CredentialModel{
						ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
						Username: "testuser123",
					}).Return(service.CredentialModel{
						Session:           service.Session{},
						ID:                "cred-aaaaaaaaaaaaaaaaaaaa",
						Name:              "oldName",
						Username:          "testuser123",
						Type:              "generic",
						Value:             "foobar",
						Description:       "",
						IsSystem:          false,
						IsHidden:          false,
						Disabled:          false,
						Visibility:        "",
						Tags:              nil,
						CreatedAt:         time.Time{},
						UpdatedAt:         time.Time{},
						UpdatedBy:         "",
						UpdatedEmulatorBy: "",
					}, nil).Once()
					storage.On("Update", types.CredUpdate{
						SessionActor:    "testuser123",
						SessionEmulator: "",
						Username:        "testuser123",
						ID:              "cred-aaaaaaaaaaaaaaaaaaaa",
						Update: service.CredentialUpdate{
							Name:        stringPtr("newName"),
							Value:       stringPtr("newVal"),
							Description: stringPtr("newDesc"),
							Disabled:    boolPtr(true),
							Visibility:  (*service.VisibilityType)(stringPtr("Default")),
							UpdateOrAddTags: map[string]string{
								"f1": "v1",
							},
							DeleteTags: map[string]struct{}{
								"f2": {},
							},
						},
					}).Return(nil).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialUpdated", service.CredentialUpdateResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
						},
						Username:     "testuser123",
						ID:           "cred-aaaaaaaaaaaaaaaaaaaa",
						Type:         "generic",
						ValueUpdated: true,
					}).Once()
					return sink
				}(),
			},
		},
		{
			name: "actor not owner",
			args: args{
				request: service.CredentialUpdateRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					Username: "diffuser123",
					ID:       "cred-aaaaaaaaaaaaaaaaaaaa",
					Update: service.CredentialUpdate{
						Name: stringPtr("newName"),
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialUpdateError", service.CredentialUpdateResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "",
							ErrorType:       "unauthorized access - only owner is allowed to update",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoUnauthorizedError("only owner is allowed to update").GetBase(),
						},
						Username:     "diffuser123",
						ID:           "cred-aaaaaaaaaaaaaaaaaaaa",
						Type:         "",
						ValueUpdated: false,
					}).Once()
					return sink
				}(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handleUpdateEvent(tt.args.request, tt.args.storage, tt.args.sink)
			tt.args.storage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func stringPtr(s string) *string {
	return &s
}

func boolPtr(b bool) *bool {
	return &b
}
