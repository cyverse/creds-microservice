package validation

import (
	"encoding/json"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
)

type OpenStackCredential struct {
	IdentityAPIVersion string `json:"OS_IDENTITY_API_VERSION" mapstructure:"OS_IDENTITY_API_VERSION"`
	RegionName         string `json:"OS_REGION_NAME" mapstructure:"OS_REGION_NAME"`
	Interface          string `json:"OS_INTERFACE" mapstructure:"OS_INTERFACE"`
	AuthURL            string `json:"OS_AUTH_URL" mapstructure:"OS_AUTH_URL"`

	ProjectDomainID   string `json:"OS_PROJECT_DOMAIN_ID" mapstructure:"OS_PROJECT_DOMAIN_ID"`
	ProjectDomainName string `json:"OS_PROJECT_DOMAIN_NAME" mapstructure:"OS_PROJECT_DOMAIN_NAME"`
	ProjectID         string `json:"OS_PROJECT_ID" mapstructure:"OS_PROJECT_ID"`
	ProjectName       string `json:"OS_PROJECT_NAME" mapstructure:"OS_PROJECT_NAME"`
	UserDomainName    string `json:"OS_USER_DOMAIN_NAME" mapstructure:"OS_USER_DOMAIN_NAME"`
	UserDomainID      string `json:"OS_USER_DOMAIN_ID" mapstructure:"OS_USER_DOMAIN_ID"`

	Username string `json:"OS_USERNAME" mapstructure:"OS_USERNAME"`
	Password string `json:"OS_PASSWORD" mapstructure:"OS_PASSWORD"`

	AuthType      string `json:"OS_AUTH_TYPE" mapstructure:"OS_AUTH_TYPE"`
	AppCredID     string `json:"OS_APPLICATION_CREDENTIAL_ID" mapstructure:"OS_APPLICATION_CREDENTIAL_ID"`
	AppCredName   string `json:"OS_APPLICATION_CREDENTIAL_NAME" mapstructure:"OS_APPLICATION_CREDENTIAL_NAME"`
	AppCredSecret string `json:"OS_APPLICATION_CREDENTIAL_SECRET" mapstructure:"OS_APPLICATION_CREDENTIAL_SECRET"`

	Token string `json:"OS_TOKEN" mapstructure:"OS_TOKEN"`
}

func validateOpenStackCredential(cred service.CredentialModel) error {
	if len(cred.Value) == 0 {
		return fmt.Errorf("openstack credential cannot be empty")
	}
	if len(cred.Value) > 4000 {
		return fmt.Errorf("openstack credential is too long")
	}

	var openstackCred OpenStackCredential
	var err error
	if err = json.Unmarshal([]byte(cred.Value), &openstackCred); err != nil {
		return fmt.Errorf("openstack credential should be a json encoded string, %w", err)
	}
	if err = checkOpenStackRequiredFields(openstackCred); err != nil {
		return err
	}
	switch openstackCred.AuthType {
	case "":
		err = validateOpenStackPasswordCred(openstackCred)
	case "v3token":
		err = validateOpenStackTokenCred(openstackCred)
	case "v3applicationcredential":
		err = validateOpenStackAppCred(openstackCred)
	default:
		return fmt.Errorf("unknown OS_AUTH_TYPE")
	}
	if err != nil {
		return err
	}
	return nil
}

func checkOpenStackRequiredFields(cred OpenStackCredential) error {
	if cred.IdentityAPIVersion == "" {
		return fmt.Errorf("OS_IDENTITY_API_VERSION is missing")
	}
	if cred.RegionName == "" {
		return fmt.Errorf("OS_REGION_NAME is missing")
	}
	if cred.Interface == "" {
		return fmt.Errorf("OS_INTERFACE is missing")
	}
	if cred.AuthURL == "" {
		return fmt.Errorf("OS_AUTH_URL is missing")
	}
	return nil
}

func validateOpenStackPasswordCred(cred OpenStackCredential) error {
	if cred.Username == "" {
		return fmt.Errorf("OS_USERNAME is missing")
	}
	if cred.Password == "" {
		return fmt.Errorf("OS_PASSWORD is missing")
	}
	return nil
}

func validateOpenStackAppCred(cred OpenStackCredential) error {
	if cred.AppCredSecret == "" {
		return fmt.Errorf("OS_APPLICATION_CREDENTIAL_SECRET is missing")
	}
	if cred.AppCredID == "" && cred.AppCredName == "" {
		return fmt.Errorf("OS_APPLICATION_CREDENTIAL_ID is missing")
	}
	return nil
}

func validateOpenStackTokenCred(cred OpenStackCredential) error {
	if cred.Token == "" {
		return fmt.Errorf("OS_TOKEN is missing")
	}
	if cred.ProjectID != "" {
		// project scoped
		// project ID is unique, thus enough to identify the project, domain is not needed
		return nil
	}
	if cred.ProjectName != "" && cred.ProjectDomainID == "" && cred.ProjectDomainName == "" {
		// project scoped
		// project name is NOT unique, thus need domain to identify the project
		return fmt.Errorf("OS_PROJECT_NAME is specified, but domain is not")
	}
	return nil
}
