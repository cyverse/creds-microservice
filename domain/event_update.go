package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/domain/validation"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
)

func handleUpdateEvent(request service.CredentialUpdateRequest, storage ports.PersistentStoragePort, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleUpdateEvent",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
		"id":       request.ID,
	})
	err := validateUpdateRequest(request)
	if err != nil {
		logger.WithError(err).Error("update request event is invalid")
		publishUpdateErrorEvent(sink, request, "", err)
		return
	}

	logger.Debug("domain is updating a credential")

	// to re-use the validation logic for creation, we need to fetch the current credential, and apply the update, and validate the "updated" credential
	cred, err := storage.Get(service.CredentialModel{
		ID:       request.ID,
		Username: request.Username,
	})
	if err != nil {
		logger.WithError(err).Error("fail to fetch credential from storage")
		publishUpdateErrorEvent(sink, request, cred.Type, err)
		return
	}
	err = validateUpdate(request, cred)
	if err != nil {
		logger.WithError(err).Error("update values in the update request is invalid")
		publishUpdateErrorEvent(sink, request, cred.Type, err)
		return
	}
	err = storage.Update(types.CredUpdate{
		SessionActor:    request.SessionActor,
		SessionEmulator: request.SessionEmulator,
		Username:        request.Username,
		ID:              request.ID,
		Update:          request.Update,
	})
	if err != nil {
		logger.WithError(err).Error("fail to update credential in storage")
		publishUpdateErrorEvent(sink, request, cred.Type, err)
		return
	}
	sink.EventCredentialUpdated(service.CredentialUpdateResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username:     request.Username,
		ID:           request.ID,
		Type:         cred.Type,
		ValueUpdated: request.Update.Value != nil,
	})
}

func validateUpdateRequest(request service.CredentialUpdateRequest) error {
	if request.Username == "" {
		return service.NewCacaoInvalidParameterError("username cannot be empty")
	}
	if request.ID == "" {
		return service.NewCacaoInvalidParameterError("credential ID cannot be empty")
	}
	if request.GetSessionActor() != request.Username {
		return service.NewCacaoUnauthorizedError("only owner is allowed to update")
	}
	return nil
}

// check if after validation the credential validates any constraints
func validateUpdate(request service.CredentialUpdateRequest, currCred service.CredentialModel) error {
	cred := types.ApplyCredentialUpdate(currCred, request.Update)
	err := validation.ValidateCredential(cred)
	if err != nil {
		return err
	}
	return nil
}

func publishUpdateErrorEvent(sink ports.OutgoingEvents, request service.CredentialUpdateRequest, credType service.CredentialType, err error) {
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		svcErr = service.NewCacaoGeneralError(err.Error())
	}
	sink.EventCredentialUpdateError(service.CredentialUpdateResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ErrorType:       err.Error(), // populate the old error type, this won't be necessary after svc client is updated to use svc error
			ErrorMessage:    "",
			ServiceError:    svcErr.GetBase(), // populate the service error
		},
		Username:     request.Username,
		ID:           request.ID,
		Type:         credType,
		ValueUpdated: false,
	})
}
