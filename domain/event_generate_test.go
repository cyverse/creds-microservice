package domain

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/ssh"
	"io"
	"os"
	"os/exec"
	"path"
	"testing"
)

// use ssh-keygen to parse the private key to make sure the key is in a format recognized by OpenSSH.
func checkGeneratedSSHKey(t *testing.T, privateKeyStr string) {
	fmt.Println(privateKeyStr)

	_, err := ssh.ParsePrivateKey([]byte(privateKeyStr))
	if !assert.NoError(t, err) {
		return
	}
	_, err = ssh.ParseRawPrivateKey([]byte(privateKeyStr))
	if !assert.NoError(t, err) {
		return
	}

	tempDir, err := os.MkdirTemp(".", "tmp_")
	if err != nil {
		panic(err)
	}
	defer os.RemoveAll(tempDir)

	privateKeyPath := path.Join(tempDir, "id_key")
	f, err := os.OpenFile(privateKeyPath, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	_, err = f.Write([]byte(privateKeyStr))
	if err != nil {
		panic(err)
	}

	cmd := exec.Command("ssh-keygen", "-f", privateKeyPath, "-y")
	stdErrPipe, err := cmd.StderrPipe()
	if err != nil {
		panic(err)
	}
	go func() {
		_, _ = io.Copy(os.Stderr, stdErrPipe)
	}()
	err = cmd.Run()
	if !assert.NoError(t, err) {
		return
	}
}

func Test_generateSSHKeyInProcess(t *testing.T) {
	if sshEnvVar, ok := os.LookupEnv("CI_INTEGRATION_SSH"); !ok || sshEnvVar != "true" {
		t.Skip("CI_INTEGRATION_SSH is not set")
		return
	}

	t.Run("ed25519", func(t *testing.T) {
		privateKeyStr, err := generateED25519SSHKey()
		if !assert.NoError(t, err) {
			return
		}
		assert.NotEmpty(t, privateKeyStr)
		checkGeneratedSSHKey(t, privateKeyStr)
	})
	t.Run("rsa_4096", func(t *testing.T) {
		privateKeyStr, err := generateRSASSHKey()
		if !assert.NoError(t, err) {
			return
		}
		assert.NotEmpty(t, privateKeyStr)
		checkGeneratedSSHKey(t, privateKeyStr)
	})
}
