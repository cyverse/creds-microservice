package domain

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/creds-microservice/ports/mocks"
	"testing"
)

func Test_handleDeleteEvent1(t *testing.T) {
	type args struct {
		request service.CredentialDeleteRequest
		storage *portsmocks.PersistentStoragePort
		sink    *portsmocks.OutgoingEvents
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "delete",
			args: args{
				request: service.CredentialDeleteRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username: "testuser-123",
					ID:       "cred-id-123",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("GetAndDelete", service.CredentialModel{ // same as request
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-id-123",
					}).Return(&service.CredentialModel{
						Session:  service.Session{},
						ID:       "cred-id-123",
						Username: "testuser-123",
						Type:     "type123",
					}, nil).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventCredentialDeleted", service.CredentialDeleteResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-id-123",
						Type:     "type123",
					})
					return out
				}(),
			},
		},
		{
			name: "actor not owner",
			args: args{
				request: service.CredentialDeleteRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username: "diff-testuser-123", // actor not owner
					ID:       "cred-id-123",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventCredentialDeleteError", service.CredentialDeleteResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
							ErrorType:       "unauthorized access - only owner is allowed to delete",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoUnauthorizedError("only owner is allowed to delete").GetBase(),
						},
						Username: "diff-testuser-123",
						ID:       "cred-id-123",
					})
					return out
				}(),
			},
		},
		{
			name: "empty username",
			args: args{
				request: service.CredentialDeleteRequest{
					Session: service.Session{
						SessionActor:    "", // actor is the same as username, but empty
						SessionEmulator: "",
					},
					Username: "",
					ID:       "cred-id-123",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventCredentialDeleteError", service.CredentialDeleteResponse{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ErrorType:       "invalid parameter error - username is empty",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoInvalidParameterError("username is empty").GetBase(),
						},
						Username: "",
						ID:       "cred-id-123",
					})
					return out
				}(),
			},
		},
		{
			name: "delete error",
			args: args{
				request: service.CredentialDeleteRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username: "testuser-123",
					ID:       "cred-id-123",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("GetAndDelete", service.CredentialModel{ // same as request
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-id-123",
					}).Return(&service.CredentialModel{Type: "type123"}, fmt.Errorf("delete operation failed")).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					out := &portsmocks.OutgoingEvents{}
					out.On("EventCredentialDeleteError", service.CredentialDeleteResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
							ErrorType:       "delete operation failed",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoGeneralError("delete operation failed").GetBase(),
						},
						Username: "testuser-123",
						ID:       "cred-id-123",
						Type:     "type123",
					})
					return out
				}(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handleDeleteEvent(tt.args.request, tt.args.storage, tt.args.sink)
			tt.args.storage.AssertExpectations(t)
			tt.args.storage.AssertExpectations(t)
		})
	}
}
