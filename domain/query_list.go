package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
)

func handleListQuery(ctx context.Context, request service.CredentialListRequest, storage ports.PersistentStoragePort) service.CredentialListReply {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleListQuery",
		"actor":    request.GetSessionActor(),
		"emulator": request.GetSessionEmulator(),
		"username": request.Filter.Username,
	})
	err := validateListQuery(request)
	if err != nil {
		return errorReplyForListQuery(request, err)
	}

	logger.Debug("domain is getting data for user")

	credList, err := storage.List(types.ListQueryData{
		Actor:    request.GetSessionActor(),
		Emulator: request.GetSessionEmulator(),
		Username: request.Filter.Username,
		Type:     request.Filter.Type,
		IsSystem: request.Filter.IsSystem,
		Disabled: request.Filter.Disabled,
		Tags:     request.Filter.Tags,
	})
	if err != nil {
		return errorReplyForListQuery(request, err)
	}
	return service.CredentialListReply{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Filter.Username,
		List:     credList,
	}
}

func validateListQuery(request service.CredentialListRequest) error {
	err := listQueryOnlyAllowOwnerAccess(request)
	if err != nil {
		return err
	}
	if request.Filter.Username == "" {
		return service.NewCacaoInvalidParameterError("username is empty")
	}
	if len(request.Filter.Username) > types.MaxCredUsernameLength {
		return service.NewCacaoInvalidParameterError("username too long")
	}
	return nil
}

func listQueryOnlyAllowOwnerAccess(queryRequest service.CredentialListRequest) error {
	if queryRequest.GetSessionActor() != queryRequest.Filter.Username {
		return service.NewCacaoUnauthorizedError("only owner of the credential is allowed to access")
	}
	return nil
}

// service.NatsSubjectCredentialsList + types.QueryReplyTypePostfix
func errorReplyForListQuery(request service.CredentialListRequest, err error) service.CredentialListReply {
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		svcErr = service.NewCacaoGeneralError(err.Error())
	}
	return service.CredentialListReply{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ErrorType:       err.Error(),
			ErrorMessage:    "",
			ServiceError:    svcErr.GetBase(),
		},
		Username: request.Filter.Username,
		List:     nil,
	}
}
