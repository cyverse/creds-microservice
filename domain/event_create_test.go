package domain

import (
	"fmt"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/creds-microservice/ports/mocks"
	"gitlab.com/cyverse/creds-microservice/types"
	"testing"
)

func Test_credentialCreationHandler_handleCreateEvent1(t *testing.T) {
	testCredentialID := common.NewID("cred")
	credIDGenerator := func() common.ID {
		return testCredentialID
	}
	type args struct {
		request         service.CredentialCreateRequest
		storage         *portsmocks.PersistentStoragePort
		sink            *portsmocks.OutgoingEvents
		credIDGenerator func() common.ID
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "empty id",
			args: args{
				request: service.CredentialCreateRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username:    "testuser-123",
					Name:        "cred-name-123",
					Value:       "{}",
					Type:        "type123",
					ID:          "", // empty ID in request
					Description: "",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Username: "testuser-123",
					}).Return([]service.CredentialModel{}, nil).Once()
					storage.On("Create", service.CredentialModel{ // same as request
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username:    "testuser-123",
						Name:        "cred-name-123",
						Value:       "{}",
						Type:        "type123",
						ID:          testCredentialID.String(),
						Description: "",
					}).Return(nil).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialAdded", service.CredentialCreateResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       testCredentialID.String(),
						Type:     "type123",
					})
					return sink
				}(),
				credIDGenerator: credIDGenerator,
			},
		},
		{
			name: "non-empty id",
			args: args{
				request: service.CredentialCreateRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username:    "testuser-123",
					Name:        "cred-name-123",
					Value:       "{}",
					Type:        "type123",
					ID:          "cred-id-123",
					Description: "",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Username: "testuser-123",
					}).Return([]service.CredentialModel{}, nil).Once()
					storage.On("Create", service.CredentialModel{ // same as request
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username:    "testuser-123",
						Name:        "cred-name-123",
						Value:       "{}",
						Type:        "type123",
						ID:          testCredentialID.String(), // non-empty ID in request
						Description: "",
					}).Return(nil).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialAdded", service.CredentialCreateResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       testCredentialID.String(),
						Type:     "type123",
					})
					return sink
				}(),
				credIDGenerator: credIDGenerator,
			},
		},
		{
			name: "actor not owner",
			args: args{
				request: service.CredentialCreateRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username:    "diff-testuser-123",
					Name:        "cred-name-123",
					Value:       "{}",
					Type:        "type123",
					ID:          "cred-id-123",
					Description: "",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialAddError", service.CredentialCreateResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
							ErrorType:       "unauthorized access - only owner is allowed to create",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoUnauthorizedError("only owner is allowed to create").GetBase(),
						},
						Username: "diff-testuser-123",
						ID:       "",
						Type:     "afb2049a80ba567d737ef5063dce8850",
					})
					return sink
				}(),
				credIDGenerator: credIDGenerator,
			},
		},
		{
			name: "bad credential, long type",
			args: args{
				request: service.CredentialCreateRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username:    "testuser-123",
					Name:        "cred-name-123",
					Value:       "{}",
					Type:        "tttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt12345",
					ID:          "cred-id-123",
					Description: "",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialAddError", service.CredentialCreateResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
							ErrorType:       "invalid parameter error - credential type too long",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoInvalidParameterError("credential type too long").GetBase(),
						},
						Username: "testuser-123",
						ID:       "",
						Type:     "c09c31dff3540e8759ad27b7c7c7764d",
					})
					return sink
				}(),
				credIDGenerator: credIDGenerator,
			},
		},
		{
			name: "bad credential, no actor",
			args: args{
				request: service.CredentialCreateRequest{
					Session: service.Session{
						SessionActor:    "", // empty actor
						SessionEmulator: "",
					},
					Username:    "",
					Name:        "cred-name-123",
					Value:       "{}",
					Type:        "type123",
					ID:          "cred-id-123",
					Description: "",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialAddError", service.CredentialCreateResponse{
						Session: service.Session{
							SessionActor:    "",
							SessionEmulator: "",
							ErrorType:       "invalid parameter error - username(owner) of the credential cannot be empty",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoInvalidParameterError("username(owner) of the credential cannot be empty").GetBase(),
						},
						Username: "",
						ID:       "",
						Type:     "afb2049a80ba567d737ef5063dce8850",
					})
					return sink
				}(),
				credIDGenerator: credIDGenerator,
			},
		},
		{
			name: "bad credential, bad cred value (type ssh)",
			args: args{
				request: service.CredentialCreateRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username:    "testuser-123",
					Name:        "cred-name-123",
					Value:       "{}", // bad cred value
					Type:        "ssh",
					ID:          "cred-id-123",
					Description: "",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialAddError", service.CredentialCreateResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
							ErrorType:       "invalid parameter error - ssh: no key found",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoInvalidParameterError("ssh: no key found").GetBase(),
						},
						Username: "testuser-123",
						ID:       "",
						Type:     "1787d7646304c5d987cf4e64a3973dc7",
					})
					return sink
				}(),
				credIDGenerator: credIDGenerator,
			},
		},
		{
			name: "create error",
			args: args{
				request: service.CredentialCreateRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username:    "testuser-123",
					Name:        "cred-name-123",
					Value:       "{}",
					Type:        "type123",
					ID:          "cred-id-123",
					Description: "",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Username: "testuser-123",
					}).Return([]service.CredentialModel{}, nil).Once()
					storage.On("Create", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username:    "testuser-123",
						Name:        "cred-name-123",
						Value:       "{}",
						Type:        "type123",
						ID:          testCredentialID.String(),
						Description: "",
					}).Return(fmt.Errorf("create operation failed")).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialAddError", service.CredentialCreateResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
							ErrorType:       "create operation failed",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoGeneralError("create operation failed").GetBase(),
						},
						Username: "testuser-123",
						ID:       testCredentialID.String(),
						Type:     "type123",
					})
					return sink
				}(),
				credIDGenerator: credIDGenerator,
			},
		},
		{
			name: "duplicate name",
			args: args{
				request: service.CredentialCreateRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username:    "testuser-123",
					Name:        "cred-name-123",
					Value:       "{}",
					Type:        "type123",
					ID:          "cred-id-123",
					Description: "",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Username: "testuser-123",
					}).Return([]service.CredentialModel{
						{
							Name: "cred-name-123", // name already exists
						},
					}, nil).Once()
					return storage
				}(),
				sink: func() *portsmocks.OutgoingEvents {
					sink := &portsmocks.OutgoingEvents{}
					sink.On("EventCredentialAddError", service.CredentialCreateResponse{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
							ErrorType:       "invalid parameter error - credential name is not unique",
							ErrorMessage:    "",
							ServiceError:    service.NewCacaoInvalidParameterError("credential name is not unique").GetBase(),
						},
						Username: "testuser-123",
						ID:       testCredentialID.String(),
						Type:     "type123",
					})
					return sink
				}(),
				credIDGenerator: credIDGenerator,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &credentialCreationHandler{}
			h.handleCreateEvent(tt.args.request, tt.args.storage, tt.args.sink, tt.args.credIDGenerator)
		})
	}
}
