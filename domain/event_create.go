package domain

import (
	"crypto/md5"
	"encoding/hex"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/domain/validation"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"sync"
)

type credentialCreationHandler struct {
	// TODO remove this lock, this is no longer needed after migrate to postgres as storage
	nameUniquenessCheckLock sync.Mutex
}

func (h *credentialCreationHandler) handleCreateEvent(request service.CredentialCreateRequest, storage ports.PersistentStoragePort, sink ports.OutgoingEvents, credIDGenerator func() common.ID) {
	request.ID = credIDGenerator().String()
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleCreateEvent",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
		"id":       request.ID,
		"type":     request.Type,
	})
	err := validateCreationRequest(service.CredentialModel(request))
	if err != nil {
		logger.WithError(err).Error("creation request event is invalid")
		typeHash := md5.New()
		typeHash.Write([]byte(request.Type))
		publishCreateErrorEvent(sink, service.CredentialCreateRequest{
			Session:  request.Session,
			Username: request.Username,
			Type:     service.CredentialType(hex.EncodeToString(typeHash.Sum(nil))),
		}, err) // don't propagate bad values in the event emitted
		return
	}

	logger.Debug("domain is creating a secret")

	credNameUnique, err := h.checkIfNameIsUnique(storage, request.Username, request.ID, request.Name)
	if err != nil {
		logger.WithError(err).Error("fail to check if credential name is unique")
		publishCreateErrorEvent(sink, request, err)
		return
	}
	if !credNameUnique {
		logger.WithError(err).Error("creation name is not unique")
		publishCreateErrorEvent(sink, request, service.NewCacaoInvalidParameterError("credential name is not unique"))
		return
	}
	err = storage.Create(service.CredentialModel(request))
	if err != nil {
		logger.WithError(err).Error("fail to create credential in storage")
		publishCreateErrorEvent(sink, request, err)
		return
	}
	sink.EventCredentialAdded(service.CredentialCreateResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Username,
		ID:       request.ID,
		Type:     request.Type,
	})
}

func validateCreationRequest(request service.CredentialModel) error {
	if request.GetSessionActor() != request.Username {
		return service.NewCacaoUnauthorizedError("only owner is allowed to create")
	}
	err := validation.ValidateCredential(request)
	if err != nil {
		return err
	}
	return nil
}

// check if credential name is unique within a user's scope. aka, the combination of username and credential name is unique.
// FIXME this uniqueness check on name is not 100% safe for concurrency, even though mutex is used, other instances of credential service will have a race condition on this.
// The reason for race condition is that there is a gap between the check and creation, thus multiple creation could interleave, result in duplicate credential name.
func (h *credentialCreationHandler) checkIfNameIsUnique(storage ports.PersistentStoragePort, username, credID, credName string) (isUnique bool, err error) {
	h.nameUniquenessCheckLock.Lock()
	defer h.nameUniquenessCheckLock.Unlock()

	credList, err := storage.List(types.ListQueryData{
		Username: username,
	})
	if err != nil {
		return false, err
	}
	for _, cred := range credList {
		if cred.ID == credID {
			// skip check against self.
			// if cred.ID == credID, then it is a "rename"
			continue
		}
		if cred.Name == credName {
			// name already exists, not unique
			return false, err
		}
	}
	return true, err
}

func publishCreateErrorEvent(sink ports.OutgoingEvents, request service.CredentialCreateRequest, err error) {
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		svcErr = service.NewCacaoGeneralError(err.Error())
	}
	sink.EventCredentialAddError(service.CredentialCreateResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ErrorType:       err.Error(), // populate the old error type, this won't be necessary after svc client is updated to use svc error
			ErrorMessage:    "",
			ServiceError:    svcErr.GetBase(), // populate the service error
		},
		Username: request.Username,
		ID:       request.ID,
		Type:     request.Type,
	})
}
