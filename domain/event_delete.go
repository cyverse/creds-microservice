package domain

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
)

func handleDeleteEvent(request service.CredentialDeleteRequest, storage ports.PersistentStoragePort, sink ports.OutgoingEvents) {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleDeleteEvent",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
		"id":       request.ID,
	})
	err := validateDeletionRequest(request)
	if err != nil {
		publishDeleteErrorEvent(sink, request, "", err)
		return
	}

	logger.Debug("domain is deleting a secret")
	cred, err := storage.GetAndDelete(service.CredentialModel{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Username,
		ID:       request.ID,
	})
	if err != nil {
		publishDeleteErrorEvent(sink, request, cred.Type, err)
		return
	}
	if cred == nil {
		publishDeleteErrorEvent(sink, request, "", service.NewCacaoNotFoundError(""))
		return
	}
	sink.EventCredentialDeleted(service.CredentialDeleteResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Username,
		ID:       request.ID,
		Type:     cred.Type,
	})
}

func validateDeletionRequest(request service.CredentialDeleteRequest) error {
	if request.GetSessionActor() != request.Username {
		return service.NewCacaoUnauthorizedError("only owner is allowed to delete")
	}
	if request.Username == "" {
		return service.NewCacaoInvalidParameterError("username is empty")
	}
	if request.ID == "" {
		return service.NewCacaoInvalidParameterError("credential ID is empty")
	}
	if len(request.Username) > types.MaxCredUsernameLength {
		return service.NewCacaoInvalidParameterError("username too long")
	}
	if len(request.ID) > types.MaxCredIDLength {
		return service.NewCacaoInvalidParameterError("credential ID too long")
	}
	return nil
}

func publishDeleteErrorEvent(sink ports.OutgoingEvents, request service.CredentialDeleteRequest, credType service.CredentialType, err error) {
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		svcErr = service.NewCacaoGeneralError(err.Error())
	}
	sink.EventCredentialDeleteError(service.CredentialDeleteResponse{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ErrorType:       err.Error(), // populate the old error type, this won't be necessary after svc client is updated to use svc error
			ErrorMessage:    "",
			ServiceError:    svcErr.GetBase(), // populate the service error
		},
		Username: request.Username,
		ID:       request.ID,
		Type:     credType,
	})
}
