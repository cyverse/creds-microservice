package domain

import (
	"context"
	"sync"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	QueryIn         ports.IncomingQueryPort
	EventsIn        ports.IncomingEventPort
	Storage         ports.PersistentStoragePort
	CredIDGenerator ports.CredIDGenerator

	creationHandler   *credentialCreationHandler
	generationHandler *systemCredentialGenerationHandler
}

// Init initializes all the specified adapters
func (d *Domain) Init(c types.Config) error {
	log.Debug("domain.Init() starting")
	err := d.Storage.Init(c)
	if err != nil {
		return err
	}
	err = d.QueryIn.Init(c)
	if err != nil {
		return err
	}
	err = d.EventsIn.Init(c)
	if err != nil {
		return err
	}
	d.creationHandler = new(credentialCreationHandler)
	d.generationHandler, err = newSystemCredentialGenerationHandler(c.CredentialGenerationRequestPerSec, c.UseOpenSSHKeyGen, c.KeyGenerationTmpDirectory)
	if err != nil {
		return err
	}
	return nil
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(ctx context.Context) {
	log.Trace("domain.Start() starting")

	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	d.QueryIn.SetHandlers(d)
	wg.Add(1)
	err := d.QueryIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Panic("fail to start query adapter")
		return
	}

	d.EventsIn.SetHandlers(d)
	wg.Add(1)
	err = d.EventsIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Panic("fail to start event adapter")
		return
	}
	wg.Wait()
}

var _ ports.IncomingEventHandlers = (*Domain)(nil)

// EventCredentialAddRequested implements ports.IncomingEventHandlers.
func (d *Domain) EventCredentialAddRequested(ctx context.Context, request service.CredentialCreateRequest, sink ports.OutgoingEvents) {
	d.creationHandler.handleCreateEvent(request, d.Storage, sink, d.CredIDGenerator)
}

// EventCredentialDeleteRequested implements ports.IncomingEventHandlers.
func (d *Domain) EventCredentialDeleteRequested(ctx context.Context, request service.CredentialDeleteRequest, sink ports.OutgoingEvents) {
	handleDeleteEvent(request, d.Storage, sink)
}

// EventCredentialUpdateRequested implements ports.IncomingEventHandlers.
func (d *Domain) EventCredentialUpdateRequested(ctx context.Context, request service.CredentialUpdateRequest, sink ports.OutgoingEvents) {
	handleUpdateEvent(request, d.Storage, sink)
}

// EventSystemCredentialGenerationRequested implements ports.IncomingEventHandlers.
func (d *Domain) EventSystemCredentialGenerationRequested(ctx context.Context, request service.CredentialCreateRequest, sink ports.OutgoingEvents) {
	d.generationHandler.handleEvent(request, d.Storage, sink, d.CredIDGenerator)
}

var _ ports.IncomingQueryHandlers = (*Domain)(nil)

// CredentialsGet implements ports.IncomingQueryHandlers
func (d *Domain) CredentialsGet(ctx context.Context, request service.CredentialGetRequest) service.CredentialGetReply {
	return handleGetQuery(ctx, request, d.Storage)
}

// CredentialsList implements ports.IncomingQueryHandlers
func (d *Domain) CredentialsList(ctx context.Context, request service.CredentialListRequest) service.CredentialListReply {
	return handleListQuery(ctx, request, d.Storage)
}
