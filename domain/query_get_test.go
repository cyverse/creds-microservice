package domain

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/creds-microservice/ports/mocks"
	"testing"
)

func Test_handleGetQuery1(t *testing.T) {
	type args struct {
		ctx     context.Context
		request service.CredentialGetRequest
		storage *portsmocks.PersistentStoragePort
	}
	tests := []struct {
		name string
		args args
		want service.CredentialGetReply
	}{
		{
			name: "empty requests",
			args: args{
				ctx:     context.TODO(),
				request: service.CredentialGetRequest{},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
			},
			want: service.CredentialGetReply{
				Session: service.Session{
					ErrorType:    "invalid parameter error - username is empty",
					ErrorMessage: "",
					ServiceError: service.NewCacaoInvalidParameterError("username is empty").GetBase(),
				},
			},
		},
		{
			name: "1 get",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialGetRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username: "testuser-123",
					ID:       "cred-123",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Get", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-123",
					}).Return(service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						Value:    "cred-value-123",
						ID:       "cred-123",
					}, nil).Once()
					return storage
				}(),
			},
			want: service.CredentialGetReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
				},
				Username: "testuser-123",
				Value:    "cred-value-123",
				ID:       "cred-123",
			},
		},
		{
			name: "1 get, actor not owner",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialGetRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username: "diff-testuser-123",
					ID:       "cred-123",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
			},
			want: service.CredentialGetReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
					ErrorType:       "unauthorized access - only owner of the credential is allowed to access", // FIXME use service error instead
					ErrorMessage:    "",
					ServiceError:    service.NewCacaoUnauthorizedError("only owner of the credential is allowed to access").GetBase(),
				},
				Username: "diff-testuser-123",
				ID:       "cred-123",
			},
		},
		{
			name: "1 get error",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialGetRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Username: "testuser-123",
					ID:       "cred-123",
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("Get", service.CredentialModel{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						ID:       "cred-123",
					}).Return(service.CredentialModel{}, fmt.Errorf("get operation failed")).Once()
					return storage
				}(),
			},
			want: service.CredentialGetReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
					ErrorType:       "get operation failed", // FIXME use service error instead
					ErrorMessage:    "",
					ServiceError:    service.NewCacaoGeneralError("get operation failed").GetBase(),
				},
				Username: "testuser-123",
				ID:       "cred-123",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, handleGetQuery(tt.args.ctx, tt.args.request, tt.args.storage), "handleGetQuery(%v, %v, %v)", tt.args.ctx, tt.args.request, tt.args.storage)
			tt.args.storage.AssertExpectations(t)
		})
	}
}
