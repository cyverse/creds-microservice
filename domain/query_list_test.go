package domain

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/creds-microservice/ports/mocks"
	"gitlab.com/cyverse/creds-microservice/types"
	"testing"
)

func Test_handleListQuery1(t *testing.T) {
	type args struct {
		ctx     context.Context
		request service.CredentialListRequest
		storage *portsmocks.PersistentStoragePort
	}
	tests := []struct {
		name string
		args args
		want service.CredentialListReply
	}{
		{
			name: "1 empty list",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialListRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Filter: service.CredentialListFilter{
						Username: "testuser-123",
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Actor:    "testuser-123",
						Emulator: "",
						Username: "testuser-123",
					}).Return([]service.CredentialModel{}, nil).Once()
					return storage
				}(),
			},
			want: service.CredentialListReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
				},
				Username: "testuser-123",
				List:     []service.CredentialModel{},
			},
		},
		{
			name: "listed 1",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialListRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Filter: service.CredentialListFilter{
						Username: "testuser-123",
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Actor:    "testuser-123",
						Emulator: "",
						Username: "testuser-123",
					}).Return([]service.CredentialModel{
						{
							Session: service.Session{
								SessionActor:    "testuser-123",
								SessionEmulator: "",
							},
							Username: "testuser-123",
							Value:    "cred-value-123",
							ID:       "cred-123",
						},
					}, nil).Once()
					return storage
				}(),
			},
			want: service.CredentialListReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
				},
				Username: "testuser-123",
				List: []service.CredentialModel{
					{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						Value:    "cred-value-123",
						ID:       "cred-123",
					},
				},
			},
		},
		{
			name: "listed 1, filtered by type",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialListRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Filter: service.CredentialListFilter{
						Username: "testuser-123",
						Type:     "ssh",
						IsSystem: nil,
						Disabled: nil,
						Tags:     nil,
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Actor:    "testuser-123",
						Emulator: "",
						Username: "testuser-123",
						Type:     "ssh",
					}).Return([]service.CredentialModel{
						{
							Session: service.Session{
								SessionActor:    "testuser-123",
								SessionEmulator: "",
							},
							Username: "testuser-123",
							Value:    "cred-value-123",
							ID:       "cred-123",
						},
					}, nil).Once()
					return storage
				}(),
			},
			want: service.CredentialListReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
				},
				Username: "testuser-123",
				List: []service.CredentialModel{
					{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						Value:    "cred-value-123",
						ID:       "cred-123",
					},
				},
			},
		},
		{
			name: "listed 1, more filters",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialListRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Filter: service.CredentialListFilter{
						Username: "testuser-123",
						Type:     "",
						IsSystem: valuePtr(true),
						Disabled: valuePtr(false),
						Tags:     map[string]string{"tag1": "value1"},
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Actor:    "testuser-123",
						Emulator: "",
						Username: "testuser-123",
						IsSystem: valuePtr(true),
						Disabled: valuePtr(false),
						Tags:     map[string]string{"tag1": "value1"},
					}).Return([]service.CredentialModel{
						{
							Session: service.Session{
								SessionActor:    "testuser-123",
								SessionEmulator: "",
							},
							Username: "testuser-123",
							Value:    "cred-value-123",
							ID:       "cred-123",
						},
					}, nil).Once()
					return storage
				}(),
			},
			want: service.CredentialListReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
				},
				Username: "testuser-123",
				List: []service.CredentialModel{
					{
						Session: service.Session{
							SessionActor:    "testuser-123",
							SessionEmulator: "",
						},
						Username: "testuser-123",
						Value:    "cred-value-123",
						ID:       "cred-123",
					},
				},
			},
		},
		{
			name: "actor not owner",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialListRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Filter: service.CredentialListFilter{
						Username: "diffuser123",
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					return storage
				}(),
			},
			want: service.CredentialListReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
					ErrorType:       "unauthorized access - only owner of the credential is allowed to access",
					ErrorMessage:    "",
					ServiceError:    service.NewCacaoUnauthorizedError("only owner of the credential is allowed to access").GetBase(),
				},
				Username: "diffuser123",
				List:     nil,
			},
		},
		{
			name: "1 list error",
			args: args{
				ctx: context.TODO(),
				request: service.CredentialListRequest{
					Session: service.Session{
						SessionActor:    "testuser-123",
						SessionEmulator: "",
					},
					Filter: service.CredentialListFilter{
						Username: "testuser-123",
					},
				},
				storage: func() *portsmocks.PersistentStoragePort {
					storage := portsmocks.NewPersistentStoragePort(t)
					storage.On("List", types.ListQueryData{
						Actor:    "testuser-123",
						Emulator: "",
						Username: "testuser-123",
					}).Return(nil, fmt.Errorf("list operation failed")).Once()
					return storage
				}(),
			},
			want: service.CredentialListReply{
				Session: service.Session{
					SessionActor:    "testuser-123",
					SessionEmulator: "",
					ErrorType:       "list operation failed",
					ErrorMessage:    "",
					ServiceError:    service.NewCacaoGeneralError("list operation failed").GetBase(),
				},
				Username: "testuser-123",
				List:     nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, handleListQuery(tt.args.ctx, tt.args.request, tt.args.storage), "handleListQuery(%v, %v, %v)", tt.args.ctx, tt.args.request, tt.args.storage)
			tt.args.storage.AssertExpectations(t)
		})
	}
}

func valuePtr[T any](v T) *T {
	return &v
}
