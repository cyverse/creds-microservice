package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
)

func handleGetQuery(ctx context.Context, request service.CredentialGetRequest, storage ports.PersistentStoragePort) service.CredentialGetReply {
	logger := log.WithFields(log.Fields{
		"package":  "domain",
		"function": "handleGetQuery",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"username": request.Username,
		"id":       request.ID,
	})
	err := validateGetQuery(request)
	if err != nil {
		logger.WithError(err).Error("get query is invalid")
		return errorReplyForGetQuery(request, err)
	}

	logger.Debug("domain is getting data for user")

	fetchedCred, err := storage.Get(service.CredentialModel{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Username: request.Username,
		ID:       request.ID,
	})
	if err != nil {
		logger.WithError(err).Error("fail to fetch cred")
		return errorReplyForGetQuery(request, err)
	} else {
		logger.WithField("value", fetchedCred.Value).Trace("fetched credential")
	}
	fetchedCred.Session = service.CopySessionActors(request.Session)
	return service.CredentialGetReply(fetchedCred)
}

func validateGetQuery(request service.CredentialGetRequest) error {
	err := getQueryOnlyAllowOwnerAccess(request)
	if err != nil {
		return err
	}
	if request.Username == "" {
		return service.NewCacaoInvalidParameterError("username is empty")
	}
	if request.ID == "" {
		return service.NewCacaoInvalidParameterError("credential ID is empty")
	}
	if len(request.Username) > types.MaxCredUsernameLength {
		return service.NewCacaoInvalidParameterError("username too long")
	}
	if len(request.ID) > types.MaxCredIDLength {
		return service.NewCacaoInvalidParameterError("credential ID too long")
	}
	return nil
}

func getQueryOnlyAllowOwnerAccess(queryRequest service.CredentialGetRequest) error {
	if queryRequest.GetSessionActor() != queryRequest.Username {
		return service.NewCacaoUnauthorizedError("only owner of the credential is allowed to access")
	}
	return nil
}

func errorReplyForGetQuery(request service.CredentialGetRequest, err error) service.CredentialGetReply {
	svcErr, ok := err.(service.CacaoError)
	if !ok {
		svcErr = service.NewCacaoGeneralError(err.Error())
	}
	return service.CredentialGetReply{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ErrorType:       err.Error(),
			ErrorMessage:    "",
			ServiceError:    svcErr.GetBase(),
		},
		Username: request.Username,
		ID:       request.ID,
	}
}
