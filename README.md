# Credential Microservice


# Env Variables
Configurations are done via environment variables

| name           | default               |
|----------------|-----------------------|
| NATS_URL       | localhost:1234        |
| NATS_CLIENT_ID | -                     |
| VAULT_ADDRESS  | http://127.0.0.1:8200 |
| VAULT_TOKEN    | (get from vault)      |
| LOG_LEVEL      | debug                 |

# Vault
- This service uses Hashicorp Vault as a storage backend for credentials.
- The KV __version 1__ (not version 2) secret engine is used.
- The KV secret engine is assumed to be enabled at `secret/` path

# Local Development Setup
### Setup Vault
- download vault binary
https://www.vaultproject.io/downloads
- 
- run Vault in dev mode with a token of your choice (here uses "foobarfoobar" as an example)
```bash
vault server -dev -dev-root-token-id=foobarfoobar
```

- set env var
```bash
export VAULT_ADDR=http://127.0.0.1:8200
export VAULT_TOKEN=foobarfoobar
```

- enable KV (version 1) engine
```bash
vault secrets enable -version=1 -path=secret/ kv
```

### Setup NATS Streaming (docker)
`cacao-cluster` is the default cluster ID used by CACAO
```bash
docker run --rm -p 4222:4222 -p 8222:8222 nats-streaming:latest -cid cacao-cluster
```

- set env var
```bash
export NATS_URL=127.0.0.1:4222
export STAN_URL=127.0.0.1:4222
```

### The rest
build the service & run

# Integration Tests
Integration test requires a vault or NATS Streaming instance running, therefore it is normally skipped unless `CI_INTEGRATION_VAULT` or `CI_INTEGRATION_STAN` env var is "true".

### To run locally
#### vault adapter
- download vault binary
- run vault in dev mode with a token of your choice (here uses "foobarfoobar" as an example)
```bash
vault server -dev -dev-root-token-id=foobarfoobar
```
- set env var
```bash
export CI_INTEGRATION_VAULT=true
export VAULT_ADDR=http://127.0.0.1:8200
export VAULT_TOKEN=foobarfoobar
```
- run go test (normal unit test will run along with integration test)
```bash
go test -v ./...
```

#### postgres adapter
- has docker installed
- run postgres in container
```bash
docker run --rm -ti -p 5432:5432 \
  -e POSTGRES_USER=cred \
  -e POSTGRES_DB=credential \
  -e POSTGRES_PASSWORD=example \
  postgres:12
```
- set env var
```shell
export POSTGRES_HOST=localhost
export POSTGRES_USER=cred
export POSTGRES_PASSWORD=example
export POSTGRES_DB=credential
export CI_INTEGRATION_PSQL=true
```
- run go test (normal unit test will run along with integration test)
```bash
go test -v ./...
```


#### query(NATS)/event(STAN) adapter (with nats-streaming docker image)
- has docker installed
- run NATS Streaming as docker container
```bash
docker run --rm -p 4222:4222 nats-streaming:latest
```
- set env var
```bash
export CI_INTEGRATION_STAN=true
export NATS_CLUSTER_ID=test-cluster
export NATS_URL=nats://localhost:4222
```
- run go test (normal unit test will run along with integration test)
```bash
go test -v ./...
```
