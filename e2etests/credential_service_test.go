//go:build e2e_integration

package e2etests

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/creds-microservice/adapters"
	"gitlab.com/cyverse/creds-microservice/domain"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"os"
	"reflect"
	"sync"
	"testing"
	"time"
)

// This end-to-end test requires a Postgres and STAN instance running, use the same env var
// for other integration tests (see README.md in repo root).
func TestCredentialService_SingleUser(t *testing.T) {
	if os.Getenv("CI_INTEGRATION_PSQL") != "true" {
		t.Skip("CI_INTEGRATION_PSQL is not 'true', skipping test")
		return
	}
	if os.Getenv("CI_INTEGRATION_STAN") != "true" {
		t.Skip("CI_INTEGRATION_STAN is not 'true', skipping test")
		return
	}

	serviceCtx, cancelService := context.WithCancel(context.Background())
	defer cancelService()
	var wg sync.WaitGroup
	wg.Add(1)
	go setupService(serviceCtx, &wg)

	// give service some time to setup
	time.Sleep(time.Second)

	var conf messaging2.NatsStanMsgConfig
	conf.ClientID = common.NewID("client").String()
	err := envconfig.Process("", &conf)
	if err != nil {
		panic(err.Error())
	}
	natsConn, err := conf.ConnectNats()
	if err != nil {
		panic(err.Error())
	}
	stanConn, err := conf.ConnectStan()
	if err != nil {
		panic(err.Error())
	}
	svcClient, err := service.NewNatsCredentialClientFromConn(&natsConn, &stanConn)
	if err != nil {
		panic(err)
	}
	testListEmpty(t, svcClient)
	testGetNonExistent(t, svcClient)
	testInsertGetDeleteGet(t, svcClient)
	testInsertListDeleteList(t, svcClient)

	time.Sleep(time.Second)
	cancelService()
	wg.Wait()
}

func setupService(ctx context.Context, wgAllDone *sync.WaitGroup) {
	defer wgAllDone.Done()
	var conf types.Config
	conf.Messaging.ClientID = common.NewID("service").String()
	err := envconfig.Process("", &conf)
	if err != nil {
		panic(err.Error())
	}
	conf.ProcessDefaults()
	conf.Override()

	queryAdapter := &adapters.QueryAdapter{}
	defer queryAdapter.Close()
	eventAdapter := &adapters.StanEventAdapter{}
	defer eventAdapter.Close()
	storage := adapters.NewPostgresStorage(time.Now)
	defer storage.Close()
	svc := domain.Domain{
		QueryIn:         queryAdapter,
		EventsIn:        eventAdapter,
		Storage:         storage,
		CredIDGenerator: ports.GenerateCredentialID,
	}
	err = svc.Init(conf)
	if err != nil {
		log.WithError(err).Panic("fail to init service")
	}
	svc.Start(ctx)
}

func getContext() context.Context {
	ctx, _ := context.WithTimeout(context.Background(), time.Second)
	return ctx
}

func testListEmpty(t *testing.T, svcClient service.CredentialClient) {
	list, err := svcClient.List(getContext(), service.Actor{
		Actor:    "testuser123",
		Emulator: "",
	}, service.CredentialListFilter{
		Username: "testuser123",
	})
	if !assert.NoError(t, err) {
		t.Fatal()
	}
	assert.Len(t, list, 0)
}

func testGetNonExistent(t *testing.T, svcClient service.CredentialClient) {
	cred, err := svcClient.Get(getContext(), service.Actor{
		Actor:    "testuser123",
		Emulator: "",
	}, "do-not-exists")
	if !assert.Error(t, err) {
		t.Fatal()
	}
	assert.NotNil(t, cred.GetServiceError())
}

func testInsertGetDeleteGet(t *testing.T, svcClient service.CredentialClient) {
	beginingOfTest := time.Now()
	credReq := service.CredentialModel{
		Session: service.Session{
			SessionActor:    "testuser123",
			SessionEmulator: "",
		},
		ID:                "cred-123", // this will be overridden by the service
		Name:              "cred-name123",
		Username:          "testuser123",
		Type:              "type123",
		Value:             "val1",
		Description:       "",
		IsSystem:          false,
		IsHidden:          false,
		Disabled:          false,
		Visibility:        "",
		Tags:              nil,
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}
	credID, err := svcClient.Add(getContext(), service.Actor{Actor: "testuser123"}, credReq)
	if !assert.NoError(t, err) {
		t.Fatal()
	}
	assert.NotEqual(t, credReq.ID, credID)                                          // service should generate a new ID
	assert.Truef(t, common.ID(credID).Validate(), "credential ID is: '%s'", credID) // new credential ID should be XID, older existing ID may still be just string
	cred, err := svcClient.Get(getContext(), service.Actor{Actor: "testuser123"}, credID)
	if !assert.NoError(t, err) {
		t.Fatal()
	}
	assert.Equal(t, credReq.Username, cred.Username)
	assert.Equal(t, credReq.Value, cred.Value)
	assert.Equal(t, credReq.Description, cred.Description)
	assert.Equal(t, credReq.Disabled, cred.Disabled)
	assert.Equal(t, credReq.Visibility, cred.Visibility)
	assert.Equal(t, len(credReq.Tags), len(cred.GetTags()))
	assert.NotEqual(t, time.Time{}, cred.CreatedAt)
	assert.Equal(t, 1, cred.CreatedAt.Compare(beginingOfTest))

	err = svcClient.Delete(getContext(), service.Actor{Actor: "testuser123"}, credID)
	if !assert.NoError(t, err) {
		t.Fatal()
	}

	// fetch again after deletion
	_, err = svcClient.Get(getContext(), service.Actor{Actor: "testuser123"}, credID)
	if !assert.Error(t, err) {
		t.Fatal()
	}
	err1, ok := err.(service.CacaoError)
	if !assert.Truef(t, ok, "%s", reflect.TypeOf(err).String()) {
		t.Fatal()
	}
	assert.Equalf(t, service.CacaoNotFoundErrorMessage, err1.StandardError(), err1.Error())
}

func testInsertListDeleteList(t *testing.T, svcClient service.CredentialClient) {
	beginingOfTest := time.Now()
	credReq := service.CredentialModel{
		Session: service.Session{
			SessionActor:    "testuser123",
			SessionEmulator: "",
		},
		ID:                "cred-123", // this will be overridden by the service
		Name:              "cred-name123",
		Username:          "testuser123",
		Type:              "type123",
		Value:             "val1",
		Description:       "",
		IsSystem:          false,
		IsHidden:          false,
		Disabled:          false,
		Visibility:        "",
		Tags:              nil,
		CreatedAt:         time.Time{},
		UpdatedAt:         time.Time{},
		UpdatedBy:         "",
		UpdatedEmulatorBy: "",
	}
	credID, err := svcClient.Add(getContext(), service.Actor{Actor: "testuser123"}, credReq)
	if !assert.NoError(t, err) {
		t.Fatal()
	}
	assert.NotEqual(t, credReq.ID, credID)                                          // service should generate a new ID
	assert.Truef(t, common.ID(credID).Validate(), "credential ID is: '%s'", credID) // new credential ID should be XID, older existing ID may still be just string
	list, err := svcClient.List(getContext(),
		service.Actor{Actor: "testuser123"},
		service.CredentialListFilter{
			Username: "testuser123",
			Type:     "some-other-type",
		})
	if !assert.NoError(t, err) {
		t.Fatal()
	}
	assert.Empty(t, list) // empty because type filter does not match
	list, err = svcClient.List(getContext(),
		service.Actor{Actor: "testuser123"},
		service.CredentialListFilter{
			Username: "testuser123",
			Type:     "type123",
		})
	if !assert.NoError(t, err) {
		t.Fatal()
	}
	if !assert.NotEmpty(t, list) {
		t.Fatal()
	}
	assert.Len(t, list, 1)
	assert.Equal(t, credReq.Username, list[0].Username)
	assert.Equal(t, "REDACTED", list[0].Value)
	assert.Equal(t, credReq.Description, list[0].Description)
	assert.Equal(t, credReq.Disabled, list[0].Disabled)
	assert.Equal(t, credReq.Visibility, list[0].Visibility)
	assert.Equal(t, len(credReq.Tags), len(list[0].GetTags()))
	assert.NotEqual(t, time.Time{}, list[0].CreatedAt)
	assert.Equal(t, 1, list[0].CreatedAt.Compare(beginingOfTest))

	err = svcClient.Delete(getContext(), service.Actor{Actor: "testuser123"}, credID)
	if !assert.NoError(t, err) {
		t.Fatal()
	}

	// list again after deletion
	list, err = svcClient.List(getContext(), service.Actor{Actor: "testuser123"}, service.CredentialListFilter{Username: "testuser123"})
	if !assert.NoError(t, err) {
		t.Fatal()
	}
	assert.Empty(t, list)
}
