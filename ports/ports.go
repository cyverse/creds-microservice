package ports

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"sync"
	"time"

	"gitlab.com/cyverse/creds-microservice/types"
)

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(c types.Config) error
}

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fulfill the purposes of both income and outgoing query ports
type IncomingQueryPort interface {
	Port
	SetHandlers(IncomingQueryHandlers)
	Start(ctx context.Context, wg *sync.WaitGroup) error
}

// IncomingQueryHandlers ...
type IncomingQueryHandlers interface {
	CredentialsGet(context.Context, service.CredentialGetRequest) service.CredentialGetReply
	CredentialsList(context.Context, service.CredentialListRequest) service.CredentialListReply
}

// IncomingEventPort is an example interface for an event port.
type IncomingEventPort interface {
	Port
	Start(context.Context, *sync.WaitGroup) error
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the event handlers for all event operations this service supports.
type IncomingEventHandlers interface {
	EventCredentialAddRequested(ctx context.Context, request service.CredentialCreateRequest, sink OutgoingEvents)
	EventCredentialDeleteRequested(ctx context.Context, request service.CredentialDeleteRequest, sink OutgoingEvents)
	EventCredentialUpdateRequested(ctx context.Context, request service.CredentialUpdateRequest, sink OutgoingEvents)
	EventSystemCredentialGenerationRequested(ctx context.Context, request service.CredentialCreateRequest, sink OutgoingEvents)
}

// OutgoingEvents is an abstraction for publishing events, each method publish an
// event corresponding to the name of the method, this interface contains all
// events that will be published by this service.
type OutgoingEvents interface {
	EventCredentialAdded(service.CredentialCreateResponse)
	EventCredentialAddError(service.CredentialCreateResponse)
	EventCredentialDeleted(service.CredentialDeleteResponse)
	EventCredentialDeleteError(service.CredentialDeleteResponse)
	EventCredentialUpdated(service.CredentialUpdateResponse)
	EventCredentialUpdateError(service.CredentialUpdateResponse)
}

// PersistentStoragePort is a port to store messages.
// The persistence storage should ensure the uniqueness of the combination of username and ID.
type PersistentStoragePort interface {
	Port
	Create(d service.CredentialModel) error
	Delete(d service.CredentialModel) error
	// GetAndDelete get current cred and delete cred, return nil, nil if credential not found
	GetAndDelete(d service.CredentialModel) (*service.CredentialModel, error)
	Get(d service.CredentialModel) (service.CredentialModel, error)
	Update(credUpdate types.CredUpdate) error
	List(d types.ListQueryData) ([]service.CredentialModel, error)
}

// TimeSrc returns the current time
type TimeSrc func() time.Time

// CredIDGenerator generates a new unique credential ID.
//
// The reason for this to exist is to extract out the logic for generating new
// credential ID, make it easier to test handlers.
type CredIDGenerator func() common.ID

// GenerateCredentialID is default implementation of CredIDGenerator
func GenerateCredentialID() common.ID {
	return common.NewID("cred")
}
