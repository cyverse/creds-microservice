image: "registry.gitlab.com/cyverse/ci-images/golang-linter:latest"

stages:
  - test
  - release

variables:
  SKAFFOLD_VERSION: "latest"
  IMAGE_RELEASE: $CI_COMMIT_REF_NAME

.non-scheduled-job:
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
      when: always

.release-job:
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
      when: always
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always

format-lint-job:
  stage: test
  image: registry.gitlab.com/cyverse/ci-images/golang-linter:latest
  script:
    - echo "==================== gofmt ===================="
    - gofmt -s -l -d .
    - test -z $(gofmt -s -l .)
    - echo "==================== golint ===================="
    - golint ./...
    - echo "==================== ineffassign ===================="
    - ineffassign .
    - echo "==================== misspell ===================="
    - misspell .
    - echo "==================== go vet ===================="
    - go vet ./...
    - echo "==================== staticcheck ===================="
    - staticcheck ./...

govulncheck:
  stage: test
  script:
    - govulncheck ./...

unit-test-job:
  stage: test
  extends: [.non-scheduled-job]
  variables:
    CI_INTEGRATION_SSH: "true"
  script:
    - which ssh-keygen
    - go test -v ./...

# integration tests that only requires a vault instance
vault-integration-test-job:
  stage: test
  extends: [.non-scheduled-job]
  services:
    - vault:1.10.4
  variables:
    CI_INTEGRATION_VAULT: "true"
    VAULT_ADDR: http://vault:8200
    VAULT_TOKEN: foobarfooar
    VAULT_DEV_ROOT_TOKEN_ID: foobarfooar
  script:
    - go test -v ./...
  needs:
    # unit test should succeed before integration test is run
    - unit-test-job

# integration tests that only requires a postgres instance
.postgres-integration-test-job:
  stage: test
  extends: [.non-scheduled-job]
  variables:
    CI_INTEGRATION_PSQL: "true"
    POSTGRES_HOST: postgres
    POSTGRES_USER: cred
    POSTGRES_DB: credential
    POSTGRES_PASSWORD: foobarfoobar
  script:
    - go test -v ./...
  needs:
    # unit test should succeed before integration test is run
    - unit-test-job

postgres-12-integration-test-job:
  extends: [.postgres-integration-test-job]
  services:
    - postgres:12
postgres-13-integration-test-job:
  extends: [.postgres-integration-test-job]
  services:
    - postgres:13
postgres-14-integration-test-job:
  extends: [.postgres-integration-test-job]
  services:
    - postgres:14
postgres-15-integration-test-job:
  extends: [.postgres-integration-test-job]
  services:
    - postgres:15
postgres-16-integration-test-job:
  extends: [.postgres-integration-test-job]
  services:
    - postgres:16

# integration tests that only requires a NATS Streaming instance
stan-integration-test-job:
  stage: test
  extends: [.non-scheduled-job]
  services:
    - nats-streaming:latest
  variables:
    CI_INTEGRATION_STAN: "true"
    NATS_URL: nats://nats-streaming:4222
    NATS_CLUSTER_ID: test-cluster
  script:
    - go test -v ./...
  needs:
  # unit test should succeed before integration test is run
    - unit-test-job

# end-to-end test
e2e-integration-test-job:
  stage: test
  extends: [.non-scheduled-job]
  services:
    - postgres:12
    - nats-streaming:latest
  variables:
    CI_INTEGRATION_PSQL: "true"
    POSTGRES_HOST: postgres
    POSTGRES_USER: cred
    POSTGRES_DB: credential
    POSTGRES_PASSWORD: foobarfoobar
    CI_INTEGRATION_STAN: "true"
    NATS_URL: nats://nats-streaming:4222
    NATS_CLUSTER_ID: test-cluster
  script:
    - cd e2etests/
    - export POSTGRES_ENCRYPTION_KEY=$(cat /dev/random | head -c 42 | base64)
    - go test -v -race . --tags e2e_integration
  needs:
    # unit test should succeed before integration test is run
    - unit-test-job


release-job:
  stage: release
  extends: [.release-job]
  image: docker
  services:
    - docker:dind
  tags:
    - dind
  before_script:
    - docker info
    - wget -O skaffold https://storage.googleapis.com/skaffold/releases/$SKAFFOLD_VERSION/skaffold-linux-amd64
    - chmod +x skaffold
    - mv skaffold /usr/local/bin
    - skaffold version
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - |
      if [ -z "$CI_COMMIT_REF_NAME" ]; then # if we cannot rely on branch or tag for whatever reason
        IMAGE_RELEASE=$CI_COMMIT_SHORT_SHA
      elif [ "$CI_COMMIT_REF_NAME" == "master" ]; then # then we're good!
        IMAGE_RELEASE="latest"
      fi
  script:
    - skaffold build --default-repo $CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE --profile prod --cache-artifacts=false --tag=git-$CI_COMMIT_SHORT_SHA # tag with git commit
    - skaffold build --default-repo $CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE --profile prod --cache-artifacts=false --tag=latest # note, the image name must = project name
    - echo "Release complete."

sast:
  stage: test
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
