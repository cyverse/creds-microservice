// Copy user credential from one vault instance to another.
// Assume that credential service uses KV version 1 secret engine, and credential are stored in path like secret/<username>/secrets/<credential-ID>.
package main

import (
	"flag"
	"fmt"
	vault "github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
	"os"
	"time"
)

const vaultNamespace = "secret/"

var (
	// change address if necessary
	srcVaultAddress  = "http://127.0.0.1:8300"
	destVaultAddress = "http://127.0.0.1:8200"
)

func main() {
	dryRun := flag.Bool("dry-run", false, "dry run, do not write to destination vault")
	flag.Parse()
	if *dryRun {
		log.Info("dry run the program, nothing will be written to the destination vault...")
	} else {
		log.Info("migration will start in 5 sec")
		time.Sleep(time.Second * 5)
		log.Info("migration starting")
	}

	var srcClient *vault.Logical
	var destClient *vault.Logical
	srcClient = newVaultClient(srcVaultAddress, os.Getenv("VAULT_TOKEN"))
	destClient = newVaultClient(destVaultAddress, os.Getenv("VAULT_TOKEN2"))

	secretPathList, err := listVaultPathForAllCredentials(srcClient)
	if err != nil {
		log.WithError(err).Fatal()
	}
	for _, secretPath := range secretPathList {
		//err = copyCredentialWithUpdatedUsername(srcClient, destClient, secretPath, *dryRun)
		err = copyCredential(srcClient, destClient, secretPath, *dryRun)
		if err != nil {
			log.WithField("path", secretPath).WithError(err).Error("fail to copy credential")
			return
		}
	}
	log.WithField("count", len(secretPathList)).Infof("successfully copied all credential")
}

func newVaultClient(vaultAddr, vaultToken string) *vault.Logical {
	config := vault.DefaultConfig()
	config.Timeout = time.Second * 3
	config.Address = vaultAddr
	client, err := vault.NewClient(config)
	if err != nil {
		log.WithField("addr", vaultAddr).WithError(err).Fatal("fail to create client")
	}
	client.SetToken(vaultToken)
	return client.Logical()
}

// return a list of vault path
func listVaultPathForAllCredentials(client *vault.Logical) ([]string, error) {
	var secretPathList []string
	userList, err := vaultList(client, vaultNamespace)
	if err != nil {
		log.WithError(err).Error("fail to list all users")
		return nil, err
	}
	log.Infof("there are %d users", len(userList))
	for _, user := range userList {
		username := user
		if user[len(user)-1] == '/' {
			username = user[:len(user)-1]
		}
		credList, err := vaultList(client, vaultUserBasePath(username))
		if err != nil {
			log.WithField("username", username).WithError(err).Error("fail to list cred for user")
			return nil, err
		}
		log.WithField("username", username).Infof("there are %d credential for user", len(credList))
		for _, credID := range credList {
			secretPathList = append(secretPathList, vaultCredentialPath(username, credID))
		}
	}
	log.Infof("there are %d credentials in total", len(secretPathList))
	return secretPathList, nil
}

func vaultList(client *vault.Logical, path string) ([]string, error) {
	var result []string
	list, err := client.List(path)
	if err != nil {
		return nil, err
	}
	if list == nil {
		return nil, fmt.Errorf("list result from vault is nil")
	}
	if list.Data == nil {
		return nil, fmt.Errorf("list data from vault is nil")
	}
	dataRaw, ok := list.Data["keys"]
	if !ok {
		return nil, fmt.Errorf("key 'keys' missing")
	}
	listRaw, ok := dataRaw.([]interface{})
	if !ok {
		return nil, fmt.Errorf("'keys' is not []interface{}")
	}
	for _, elem := range listRaw {
		subPath, ok := elem.(string)
		if !ok {
			return nil, fmt.Errorf("element in 'keys' is not string")
		}
		result = append(result, subPath)
	}
	return result, nil
}

func vaultUserBasePath(owner string) string {
	return vaultNamespace + owner + "/secrets/"
}

func vaultCredentialPath(owner, credID string) string {
	return vaultNamespace + owner + "/secrets/" + credID
}
