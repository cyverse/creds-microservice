# v1_to_v2

migrate the schema from v1 to v2.

- Add `SchemaVersion` field
- Add `Disabled` field
- Add `Name` field
    - copy `ID` field into `Name` field
- Add `TagsV2` field
    - migrate from `Tags`([]string) to map[string]string

