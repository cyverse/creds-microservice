// Assume that credential service uses KV version 1 secret engine, and credential are stored in path like secret/<username>/secrets/<credential-ID>.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	vault "github.com/hashicorp/vault/api"
	log "github.com/sirupsen/logrus"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

const vaultNamespace = "secret/"

func main() {
	dryRun := flag.Bool("dry-run", false, "dry run, do not write to destination vault")
	overwriteNameField := flag.Bool("overwrite-name", false, "overwrite name field (copy ID into Name), even if Name field is already present")
	flag.Parse()
	if *dryRun {
		log.Info("dry run the program, nothing will be written to the destination vault...")
	} else {
		log.Info("migration will start in 5 sec")
		time.Sleep(time.Second * 5)
		log.Info("migration starting")
	}

	var srcClient *vault.Logical
	srcClient = newVaultClient(os.Getenv("VAULT_ADDR"), os.Getenv("VAULT_TOKEN"))

	secretPathList, err := listVaultPathForAllCredentials(srcClient)
	if err != nil {
		log.WithError(err).Fatal()
	}
	for _, secretPath := range secretPathList {
		err = migrate(srcClient, secretPath, *dryRun, *overwriteNameField)
		if err != nil {
			log.WithField("path", secretPath).WithError(err).Error("fail to migrate secret")
			return
		}
	}
	log.WithField("count", len(secretPathList)).Infof("successfully copied all credential")
}

func newVaultClient(vaultAddr, vaultToken string) *vault.Logical {
	config := vault.DefaultConfig()
	config.Timeout = time.Second * 3
	config.Address = vaultAddr
	client, err := vault.NewClient(config)
	if err != nil {
		log.WithField("addr", vaultAddr).WithError(err).Fatal("fail to create client")
	}
	client.SetToken(vaultToken)
	return client.Logical()
}

// return a list of vault path
func listVaultPathForAllCredentials(client *vault.Logical) ([]string, error) {
	var secretPathList []string
	userList, err := vaultList(client, vaultNamespace)
	if err != nil {
		log.WithError(err).Error("fail to list all users")
		return nil, err
	}
	log.Infof("there are %d users", len(userList))
	for _, user := range userList {
		username := user
		if user[len(user)-1] == '/' {
			username = user[:len(user)-1]
		}
		credList, err := vaultList(client, vaultUserBasePath(username))
		if err != nil {
			log.WithField("username", username).WithError(err).Error("fail to list cred for user")
			return nil, err
		}
		log.WithField("username", username).Infof("there are %d credential for user", len(credList))
		for _, credID := range credList {
			secretPathList = append(secretPathList, vaultCredentialPath(username, credID))
		}
	}
	log.Infof("there are %d credentials in total", len(secretPathList))
	return secretPathList, nil
}

func vaultList(client *vault.Logical, secretPath string) ([]string, error) {
	var result []string
	list, err := client.List(secretPath)
	if err != nil {
		return nil, err
	}
	if list == nil {
		return nil, fmt.Errorf("list result from vault is nil")
	}
	if list.Data == nil {
		return nil, fmt.Errorf("list data from vault is nil")
	}
	dataRaw, ok := list.Data["keys"]
	if !ok {
		return nil, fmt.Errorf("key 'keys' missing")
	}
	listRaw, ok := dataRaw.([]interface{})
	if !ok {
		return nil, fmt.Errorf("'keys' is not []interface{}")
	}
	for _, elem := range listRaw {
		subPath, ok := elem.(string)
		if !ok {
			return nil, fmt.Errorf("element in 'keys' is not string")
		}
		result = append(result, subPath)
	}
	return result, nil
}

func vaultUserBasePath(owner string) string {
	return vaultNamespace + owner + "/secrets/"
}

func vaultCredentialPath(owner, credID string) string {
	return vaultNamespace + owner + "/secrets/" + credID
}

func migrate(client *vault.Logical, secretPath string, dryRun bool, overwriteNameField bool) error {
	secret, err := client.Read(secretPath)
	if err != nil {
		return err
	}

	err = addNameField(secret, secretPath, overwriteNameField)
	if err != nil {
		return err
	}
	err = addDisabledField(secret, secretPath)
	if err != nil {
		return err
	}
	err = addSchemaField(secret, secretPath)
	if err != nil {
		return err
	}
	err = addKeyValueTags(secret, secretPath)
	if err != nil {
		return err
	}

	log.Info(secretPath)
	log.Info(secret.Data["Name"])
	log.Info(secret.Data["Disabled"])
	log.Info(secret.Data["SchemaVersion"])
	log.Info(secret.Data["TagsV2"])
	if dryRun {
		return nil
	}

	// write the secret back to vault
	_, err = client.Write(secretPath, secret.Data)
	if err != nil {
		return err
	}
	return nil
}

func addNameField(secret *vault.Secret, secretPath string, overwriteNameField bool) error {
	existingValue, nameExists := secret.Data["Name"]
	if nameExists {
		_, isString := existingValue.(string)
		if !isString {
			return fmt.Errorf("field Name is not string (%v)", existingValue)
		}
		if !overwriteNameField {
			log.Infof("%s already has Name\n", secretPath)
			return nil
		}
	}

	name := idFromPath(secretPath)
	if name == "" {
		panic("name should not be empty")
	}
	secret.Data["Name"] = name

	return nil
}

func idFromPath(secretPath string) string {
	_, id := path.Split(secretPath)
	return id
}

func addDisabledField(secret *vault.Secret, secretPath string) error {
	if existingValue, ok := secret.Data["Disabled"]; ok {
		if _, ok := existingValue.(bool); !ok {
			return fmt.Errorf("field Disabled is not a boolean value (%v)", existingValue)
		}
		log.Infof("%s already has Disabled\n", secretPath)
		return nil
	}

	secret.Data["Disabled"] = false
	return nil
}

func addSchemaField(secret *vault.Secret, secretPath string) error {
	if existingValue, ok := secret.Data["SchemaVersion"]; ok {
		if versionNumStr, ok := existingValue.(json.Number); !ok {
			return fmt.Errorf("field SchemaVersion is not number (%v)", existingValue)
		} else {
			versionNum, err := strconv.ParseInt(string(versionNumStr), 10, 32)
			if err != nil {
				return fmt.Errorf("fail to parse field SchemaVersion as integer, %w", err)
			}
			if versionNum == 0 {
				return fmt.Errorf("SchemaVersion cannot be 0")
			}
		}
		log.Infof("%s already has SchemaVersion\n", secretPath)
		return nil
	}

	secret.Data["SchemaVersion"] = 2
	return nil
}

func addKeyValueTags(secret *vault.Secret, secretPath string) error {
	if existingValue, ok := secret.Data["TagsV2"]; ok {
		if _, ok := existingValue.(map[string]interface{}); !ok {
			return fmt.Errorf("field TagsV2 is not map[string]interface{} (%v)", existingValue)
		}
		log.Infof("%s already has TagsV2\n", secretPath)
		return nil
	}
	secret.Data["TagsV2"] = make(map[string]string)

	tagsV2 := make(map[string]string)

	tagsV1Raw, ok := secret.Data["Tags"]
	if !ok {
		log.WithField("secretPath", secretPath).Debug("no Tags, skipping")
		return nil
	}
	tagsV1String, ok := tagsV1Raw.(string)
	if !ok {
		return fmt.Errorf("tags v1 is not string")
	}
	for _, tagName := range splitTagsV1(tagsV1String) {
		tagsV2[tagName] = ""
	}
	secret.Data["TagsV2"] = tagsV2

	return nil
}

const v1SecretTagDivider = ":~`;;-~!!:"

func splitTagsV1(tagsV1 string) []string {
	if len(tagsV1) == 0 {
		return []string{}
	}
	var tags = make([]string, 0)
	split := strings.Split(tagsV1, v1SecretTagDivider)
	for _, tag := range split {
		if tag != "" {
			// only non-empty tag
			tags = append(tags, tag)
		}
	}
	return tags
}
