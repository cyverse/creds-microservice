package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/creds-microservice/adapters"
	"gitlab.com/cyverse/creds-microservice/domain"
	"gitlab.com/cyverse/creds-microservice/ports"
	"gitlab.com/cyverse/creds-microservice/types"
	"io"
	"time"
)

func main() {

	log.SetReportCaller(true)

	serviceCtx, cancel := context.WithCancel(context.Background())
	defer cancel()
	common.CancelWhenSignaled(cancel)
	defer log.Warn("service shutting down")

	var config = readConfigFromEnv()

	queryAdapter := adapters.NewNATSQueryAdapter()
	defer logCloserError(queryAdapter)

	stanAdapter := &adapters.StanEventAdapter{}
	defer logCloserError(stanAdapter)

	postgresStorage := adapters.NewPostgresStorage(utcTimeSrc)
	defer postgresStorage.Close()

	dmain := domain.Domain{
		QueryIn:         queryAdapter,
		EventsIn:        stanAdapter,
		Storage:         postgresStorage,
		CredIDGenerator: ports.GenerateCredentialID,
	}
	err := dmain.Init(config)
	if err != nil {
		log.WithError(err).Error("fail to init service")
		return
	}

	dmain.Start(serviceCtx)
}

func readConfigFromEnv() types.Config {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.WithError(err).Fatal("fail to read config from env var")
	}
	config.ProcessDefaults()
	config.Override()
	return config
}

func utcTimeSrc() time.Time {
	return time.Now().UTC()
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
		return
	}
}
